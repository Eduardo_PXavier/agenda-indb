@extends('layouts.app')

@section('content')

<!-- <head>
    <style>
        body {
            overflow: hidden;
        }
    </style>
</head> -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12 ">
            <form class="contato-needs-validation rounded myForm" method="POST" action="/contato" id="form" onsubmit="return validaCPF()" novalidate>
                {!! csrf_field() !!}
                @include('flash')
                <h4>Novo Contato:</h4>
                <div class="form-row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Nome" name="name" required>
                            <div class="valid-feedback">
                                Válido!
                            </div>
                            <div class="invalid-feedback">
                                Nome é necessário!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="last_name" placeholder="Sobre Nome" name="last_name" required>
                            <div class="valid-feedback">
                                Válido!
                            </div>
                            <div class="invalid-feedback">
                                Sobre Nome é necessário!
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-12">
                        <div class="contacts row">
                            <div class="col-4">
                                <div class="form-group multiple-form-group input-group">
                                    <input type="text" class="form-control rounded-left masktel" id="telefone" placeholder="Telefone" name="tel[]" required>
                                    <span class="input-group-btn rounded-right">
                                        <button type="button" class="btn btn-right btn-success btn-add rounded-right">+</button>
                                    </span>
                                    <div class="valid-feedback">
                                        Válido!
                                    </div>
                                    <div class="invalid-feedback">
                                        Telefone é necessário!
                                    </div>
                                </div>

                            </div>
                            <div class="col-4">
                                <div class="form-group multiple-form-group input-group">
                                    <input type="text" class="form-control" id="category" name="category[]" placeholder="Categoria" value="Amigo">
                                    <span class="input-group-btn rounded-right">
                                        <button type="button" class="btn btn-right btn-success btn-add rounded-right">+</button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group multiple-form-group input-group">
                                    <input type="cpf" class="form-control maskcpf fcpf" id="cpfNovo" name="cpf[]" placeholder="CPF" value="">
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="form-group multiple-form-group input-group">
                                    <div class="input-group-prepend" style="max-height: 38px">
                                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                                    </div>
                                    <input type="email" class="form-control" id="email" placeholder="Email" aria-describedby="inputGroupPrepend" name="email[]">
                                    <span class="input-group-btn rounded-right">
                                        <button type="button" class="btn btn-right btn-success btn-add rounded-right">+</button>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="form-row" onsubmit="return false">
                    <div class="form-group multiple-form-group input-group col-12" id="address">
                        <div class="no-padding col-md-12 text-center">
                            <span class="input-group-btn rounded-right">
                                <button type="button" class="btn btn-success btn-add rounded-right">Novo Endereço</button>
                            </span>
                        </div>
                        <div class="no-padding col-md-3 mb-3">
                            <input type="text" class="form-control form-cep" onblur="pesquisacep(this, this.value);" id="cep" name="cep[]" onkeypress="$(this).mask('00000000')" placeholder="CEP">
                        </div>
                        <div class="no-padding col-md-6 mb-3">
                            <input type="text" class="form-control" id="city" name="city[]" placeholder="Cidade">
                        </div>
                        <div class="no-padding col-md-3 mb-3">
                            <input type="text" class="form-control" id="state" name="state[]" placeholder="Estado">
                        </div>
                        <div class="no-padding col-md-12 mb-3">
                            <input type="text" class="form-control" id="street" name="street[]" placeholder="Rua">
                        </div>
                        <div class="no-padding col-md-7 mb-3">
                            <input type="text" class="form-control" id="neighborhood" name="neighborhood[]" placeholder="Bairro">
                        </div>
                        <div class="no-padding col-md-5 mb-3">
                            <input type="text" class="form-control" id="number" name="number[]" placeholder="Número">
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group form-group--no-bottom">
                            <a href="terms2"> Termos e Condições </a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="form-group form-group--no-bottom ">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                <label class="form-check-label" for="invalidCheck">
                                    Agree to terms and conditions
                                </label>
                                <div class="invalid-feedback">
                                    You must agree before submitting.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary" type="submit">Criar novo contato</button>

            </form>
        </div>
    </div>
</div>
@if (count($errors) >0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<!-- script-validacao -->
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('contato-needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        $('.masktel').mask('(00) 00 0000-0000');
                        $('.maskcpf').mask('000.000.000-00');
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
<!-- script-botao-add -->
<script>
    (function($) {
        $(function() {

            var addFormGroup = function(event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                var $formGroupClone = $formGroup.clone();

                $(this)
                    .toggleClass('btn-success btn-add btn-danger btn-remove')
                    .html('–');

                $formGroupClone.find('input').val('');

                $formGroupClone.insertBefore($formGroup);

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', true);
                }
                $('.masktel').mask('(00) 00 0000-0000');
            };

            var removeFormGroup = function(event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', false);
                }

                $formGroup.remove();
            };


            var countFormGroup = function($form) {
                return $form.find('.form-group').length;
            };

            $(document).on('click', '.btn-add', addFormGroup);
            $(document).on('click', '.btn-remove', removeFormGroup);


        });
    })(jQuery);
</script>
<!-- script-api-cep -->
<script type="text/javascript">
    //Foca no Cep
    function pesquisacep(input, value) {
        // var idEnd = valor.closest('input').data('id');

        console.log(input);
        var endereco = input.closest('div.form-group');
        console.log(endereco);
        var cep = value;
        console.log(cep);
        ajax = new XMLHttpRequest();

        var url = "http://viacep.com.br/ws/" + cep + "/json/";
        ajax.open('GET', url, true);
        ajax.send();

        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200) {

                var json = JSON.parse(ajax.responseText);
                endereco.getElementsByTagName('input')[0].value = cep;
                endereco.getElementsByTagName('input')[1].value = json.localidade;
                endereco.getElementsByTagName('input')[2].value = json.uf;
                endereco.getElementsByTagName('input')[3].value = json.logradouro;
                endereco.getElementsByTagName('input')[4].value = json.bairro;
                endereco.getElementsByTagName('input')[5].focus();

            }
        }
    }
</script>
<script>
    $(document).ready(function() {
        $('.masktel').mask('(00) 00 0000-0000');
        $('.maskcpf').mask('000.000.000-00');
    });
    $("#form").submit(function() {
        $('.masktel').unmask();
        $('.maskcpf').unmask();
    })
</script>
<!-- valida cpf -->
<script type="text/javascript">
    function validaCPF() {
        $('#cpfNovo').unmask();
        cpf = document.getElementById("cpfNovo").value;
        
        var numeros, digitos, soma, i, resultado, digitos_iguais;
        digitos_iguais = 1;
        console.log(cpf);
        if (cpf!="") {
            console.log('cpf');
            if (cpf.length < 11) {
                $('.masktel').mask('(00) 00 0000-0000');
                $('.maskcpf').mask('000.000.000-00');
                alert('CPF INVÁLIDO');
                return false;
            }
            for (i = 0; i < cpf.length - 1; i++)
                if (cpf.charAt(i) != cpf.charAt(i + 1)) {
                    digitos_iguais = 0;
                    break;
                }
            if (!digitos_iguais) {
                numeros = cpf.substring(0, 9);
                digitos = cpf.substring(9);
                soma = 0;
                for (i = 10; i > 1; i--) {
                    soma += numeros.charAt(10 - i) * i;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0)) {
                    $('.masktel').mask('(00) 00 0000-0000');
                    $('.maskcpf').mask('000.000.000-00');
                    alert('CPF INVÁLIDO');
                    return false;
                }
                numeros = cpf.substring(0, 10);
                soma = 0;
                for (i = 11; i > 1; i--) {
                    soma += numeros.charAt(11 - i) * i;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1)) {
                    $('.masktel').mask('(00) 00 0000-0000');
                    $('.maskcpf').mask('000.000.000-00');
                    alert('CPF INVÁLIDO');
                    return false;
                }
                return true;
            } else {
                $('.masktel').mask('(00) 00 0000-0000');
                $('.maskcpf').mask('000.000.000-00');
                alert('CPF INVÁLIDO');
                return false;
            }
        }
    }
</script>
@stop

