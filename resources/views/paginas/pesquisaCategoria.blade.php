@extends('layouts.app')


@section('content')

<div class="container-fluid">
    <div class="row">
            <div class="col-12 ">
                {!! csrf_field() !!}
                @include('flash')
                <h1 class="rounded-top" style="background: #504f4f54;padding: 
                    2px;margin-bottom: 0px;margin-top: 10px;color: #000;">
                    Contatos: #{{$categoria->category}}
                </h1>
                
                    <table class="table table-bordered table-striped table-hover category-table">
                        <thead class="thead-dark">
                            <tr>
                                <th class="textCenter">
                                    Nome:
                                </th>
                                <th class="textCenter">
                                    Telefone:
                                </th>
                                <th class="textCenter">
                                    Excluir:
                                </th>
                            </tr>
                        </thead>
                        @foreach($contacts as $contact)
                            <tbody class="background-contato-cetegoria">
                                <tr class="btnDelete" id="{{$contact->id}}" data-id="{{$contact->id}}">
                                        <td class="textCenter">
                                            <a class="nav-link contato-link link-contato" href="{{route('contato.edit',$contact->id)}}">
                                                <p>{{ $contact->name }}
                                                    <span style="font-size: 14px"> {{ $contact->last_name }}</span>  
                                                </p> 
                                            </a>
                                        </td>
                                        <td class="textCenter" style="font-size: 22px;">
                                            @if(is_null($contact->tels->first()))
                                                ----
                                            @else
                                                {{ $contact->tels->first()->tel}}
                                            @endif
                                        </td>
                                    
                                    <td class="textCenter">
                                        <!-- <button class="btn btn-danger delete-contact" value="{{$contact->id}}">Deletar</button>  -->
                                        <button type="button" class="btn btn-danger-tabela btnDelete" data-toggle="modal" data-id="" data-target="#myModal">Delete</button>
                                    </td>
                                
                                </tr>
                            </tbody>
                        @endforeach
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Deletar?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        tem certeza que deseja enviar contato para lixeira?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>                                            
                                        <!-- <button class="btn btn-danger delete-contact" value="">Deletar</button>  -->
                                        <button type="button" class="btn btn-danger delete-contact" value="" id="delete-btn">Deletar</button>   
                                    </div>
                                </div>
                            </div>
                        </div>
                </table>
            </div> 
        </div>
    </div>
</div>  



<script>

$('button.btnDelete').on('click', function (e) {
    e.preventDefault();
    console.log('teste');
    var idContato = $(this).closest('tr').data('id');
    console.log(idContato);
    
    $('.delete-contact').click(function () {
        var id = idContato;
        console.log(id);
        console.log('teste');   
        $.ajax({
            type:"POST",
            url:'/contato/' + id,
            data:{_method: 'delete', _token : "{{ csrf_token() }}"},
            success: function (data) {
                console.log(data);
                $("#"+id).remove();
                $('#myModal').modal('hide');
                
            },
            error: function (data) {
                console.log('Error:', data);
            }

        });

    });
});


</script>
@stop