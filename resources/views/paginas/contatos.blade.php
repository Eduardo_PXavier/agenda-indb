@extends('layouts.app')


@section('content')



<div class="col-12 container-fluid ">
    <div class="row">
        
        <div class="col-12 col-sm-12 table-responsive "style="overflow-x:auto;">
            {!! csrf_field() !!}
            @include('flash')
            <h1 class="rounded-top " style="background: #054a459e;padding: 
                    2px;margin-bottom: 0px;margin-top: 10px; color:#000000;">
                    Contatos:
                <a type="button" class="btn btn-success rounded"  href="contact/import/google">IMPORTAR DO GOOGLE</a>                
                <a type="button" class="btn btn-warning rounded"  href="excel">EXPORTAR</a>
            </h1>
            
            <table class="table table-bordered table-striped ">
                <thead class="thead-dark">
                    <tr>
                        <th class="textCenter">
                            Nome:
                        </th>
                        <th class="textCenter">
                            Telefone:
                        </th>
                        <th class="textCenter">
                            Excluir:
                        </th>
                    </tr>
                </thead>
                @foreach($contacts as $contact)
                <tbody class="background-contatos">
                    <tr class="btnDelete" id="{{$contact->id}}" data-id="{{$contact->id}}">
                        <td class="textCenter">
                            <a class="nav-link contato-link link-contato" href="contato/{{$contact->id}}/edit">
                                <p>{{ $contact->name }}
                                    <span style="font-size: 14px"> {{ $contact->last_name }}</span>
                                </p>
                            </a>
                        </td>
                        <td class="textCenter" name="tel" style="font-size: 22px;">
                            @if(is_null($contact->tels->first()))
                            ----
                            @else
                                @if($contact->tels->first()->tel!='xxxx')                                
                                <div class="masktelcontatos">
                                    {{ $contact->tels->first()->tel}}
                                </div>
                                @else
                                    {{ $contact->tels->first()->tel}}
                                @endif
                            @endif
                        </td>

                        <td class="textCenter">
                            <!-- <button class="btn btn-danger delete-contact" value="{{$contact->id}}">Deletar</button>  -->
                            <button type="button" class="btn btn-danger-tabela btnDelete" data-toggle="modal" data-id="" data-target="#myModal">Delete</button>
                        </td>

                    </tr>
                </tbody>
                @endforeach
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Deletar?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                tem certeza que deseja enviar contato para lixeira?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <!-- <button class="btn btn-danger delete-contact" value="">Deletar</button>  -->
                                <button type="button" class="btn btn-danger delete-contact" value="" id="delete-btn">Deletar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </table>
            
        </div>
    </div>
</div>
<!-- </div> -->


@stop