@extends('layouts.app')


@section('content')

<div class="container">
  @include('flash')
  <h4>
    Categorias:
  </h4>
  <div class="row">

    @foreach($categorias as $categoria)
    <div class="col-md-3 text-center" style="padding-top: 20px;">
      <a type="button" class="btn btn-primary rounded" href="contatos/{{$categoria->category}}">{{$categoria->category}}</a>
    </div>

    @endforeach
  </div>
</div>
@stop