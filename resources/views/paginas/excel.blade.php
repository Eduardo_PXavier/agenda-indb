@extends('layouts.app')


@section('content')
<div class="container-fluid">
    <div class="row">
        {!! csrf_field() !!}
        @include('flash')
        <h1 class="rounded-top col-12" style="background: #054a454d;padding: 
                    2px;margin-bottom: 0px;margin-top: 10px; color:#91fff2; ">
            Exportando contatos para excel:<a type="button" class="btn btn-success rounded" style="float: right;" href="contacts/export/">EXPORTAR</a>                
        </h1>

        <table class="table table-bordered table-striped table-hover category-table">
            <thead class="thead-dark">
                <tr>
                    <th class="textCenter">
                        Nome:
                    </th>
                    <th class="textCenter">
                        Sobrenome:
                    </th>
                    <th class="textCenter">
                        Cpf:
                    </th>
                    <th class="textCenter">
                        Dado:
                    </th>
                    <th class="textCenter">
                        valor
                    </th>
                </tr>
            </thead>
            @foreach($contatos as $contato)
            <tbody class="background-contatos">
                @foreach($contato->tels as $tel)

                <tr class="btnDelete" id="" data-id="">
                    <td class="textCenter">
                        <p>
                            {{$contato->name}}
                        </p>

                    </td>
                    <td class="textCenter" style="font-size: 14px;">
                        {{$contato->last_name}}
                    </td>
                    <td class="textCenter" style="font-size: 14px;">
                        @if($contato->cpfs->first())
                        {{$contato->cpfs->first()->cpf}}
                        @endif
                    </td>
                    <td class="textCenter">
                        Telefone
                    </td>
                    <td class="textCenter">
                        {{$tel->tel}}
                    </td>
                </tr>

                @endforeach
                @foreach($contato->emails as $email)
            
                <tr class="btnDelete" id="" data-id="">
                    <td class="textCenter">
                        <p>
                            {{$contato->name}}
                        </p>

                    </td>
                    <td class="textCenter" style="font-size: 14px;">
                        {{$contato->last_name}}
                    </td>
                    <td class="textCenter" style="font-size: 14px;">
                        @if($contato->cpfs->first())
                        {{$contato->cpfs->first()->cpf}}
                        @endif
                    </td>
                    <td class="textCenter">
                        Email
                    </td>
                    <td class="textCenter">
                        {{$email->email}}
                    </td>
                </tr>
                @endforeach
                @foreach($contato->addresses as $address)            
                <tr class="btnDelete" id="" data-id="">
                    <td class="textCenter">
                        <p>
                            {{$contato->name}}
                        </p>

                    </td>
                    <td class="textCenter" style="font-size: 14px;">
                        {{$contato->last_name}}
                    </td>
                    <td class="textCenter" style="font-size: 14px;">
                        @if($contato->cpfs->first())
                        {{$contato->cpfs->first()->cpf}}
                        @endif
                    </td>
                    <td class="textCenter">
                        Endereço
                    </td>
                    <td class="textCenter">
                        {{$address->state}}, {{$address->city}}, {{$address->neighborhood}}, {{$address->street}}, {{$address->number}}, {{$address->cep}}
                    </td>
                </tr>
                @endforeach
                @if(($contato->categories->isNotEmpty()))
                <tr class="btnDelete" id="" data-id="">
                    <td class="textCenter">
                        <p>
                            {{$contato->name}}
                        </p>

                    </td>
                    <td class="textCenter" style="font-size: 14px;">
                        {{$contato->last_name}}
                    </td>
                    <td class="textCenter" style="font-size: 14px;">
                        @if($contato->cpfs->first())
                        {{$contato->cpfs->first()->cpf}}
                        @endif
                    </td>
                    <td class="textCenter">
                        categoria
                    </td>
                    <td class="textCenter">
                        @foreach($contato->categories as $categoria)
                            {{$categoria->category}};
                        @endforeach
                    </td>
                </tr>
                @endif
            </tbody>
            @endforeach

        </table>
    </div>
</div>
</div>
</div>
<script>

</script>
@stop