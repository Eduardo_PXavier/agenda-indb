<div class="container-fluid">
    <div class="row">
        {!! csrf_field() !!}
        <table class="table table-bordered table-striped table-hover category-table">
            <thead class="thead-dark">
                <tr>
                    <th>
                        Nome:
                    </th>
                    <th>
                        Sobrenome:
                    </th>
                    <th>
                        Cpf:
                    </th>
                    <th>
                        Dado:
                    </th>
                    <th>
                        valor
                    </th>
                </tr>
            </thead>
            @foreach($contatos as $contato)
            <tbody class="background-contatos">
                @foreach($contato->tels as $tel)
                <tr>
                    <td>
                        <p>
                            {{$contato->name}}
                        </p>

                    </td>
                    <td>
                        {{$contato->last_name}}
                    </td>
                    <td>
                        @if($contato->cpfs->first())
                        {{$contato->cpfs->first()->cpf}}
                        @endif
                    </td>
                    <td>
                        Telefone
                    </td>
                    <td>
                        {{$tel->tel}}
                    </td>
                </tr>
                @endforeach
                @foreach($contato->emails as $email)
                <tr>
                    <td>
                        <p>
                            {{$contato->name}}
                        </p>

                    </td>
                    <td>
                        {{$contato->last_name}}
                    </td>
                    <td>
                        @if($contato->cpfs->first())
                        {{$contato->cpfs->first()->cpf}}
                        @endif
                    </td>
                    <td>
                        Email
                    </td>
                    <td>
                        {{$email->email}}
                    </td>
                </tr>
                @endforeach
                @foreach($contato->addresses as $address)
                <tr>
                    <td>
                        <p>
                            {{$contato->name}}
                        </p>

                    </td>
                    <td>
                        {{$contato->last_name}}
                    </td>
                    <td>
                        @if($contato->cpfs->first())
                        {{$contato->cpfs->first()->cpf}}
                        @endif
                    </td>
                    <td>
                        Endereço
                    </td>
                    <td>
                        {{$address->state}}, {{$address->city}}, {{$address->neighborhood}}, {{$address->street}}, {{$address->number}}, {{$address->cep}}
                    </td>
                </tr>
                @endforeach
                @if(($contato->categories->isNotEmpty()))
                <tr>
                    <td>
                        <p>
                            {{$contato->name}}
                        </p>
                    </td>
                    <td>
                        {{$contato->last_name}}
                    </td>
                    <td>
                        @if($contato->cpfs->first())
                        {{$contato->cpfs->first()->cpf}}
                        @endif
                    </td>
                    <td>
                        categoria
                    </td>
                    <td>
                        @foreach($contato->categories as $categoria)
                        {{$categoria->category}};
                        @endforeach
                    </td>
                </tr>
                @endif
            </tbody>
            @endforeach

        </table>
    </div>
</div>

