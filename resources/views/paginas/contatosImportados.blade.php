@extends('layouts.app')


@section('content')

<div class="container-fluid">
    <div class="row">

        
        <form class="rounded col-12" method="POST" action="/contatos/google" novalidate>
        <h1 class="rounded-top col-12" style="background: #4c793b7a;padding: 
                    2px;margin-bottom: 0px;margin-top: 10px; color:#32524f; ">
            Contatos do Google:
        </h1>
        {!! csrf_field() !!}
            <h6 class=" col-12" style="background: #5152517a;padding: 
                    2px;margin-bottom: 0px; color:#243a38;">
                Selecione os Contatos a serem adionados:
                <br>
                <button class="btn btn-primary" type="submit">Salvar</button>
                <button class="btn btn-warning btnTroca" type="buttom">Trocar Usuário</button>
                <button class="btn btn-danger btnCancelar" type="buttom">Cancelar</button>
            </h6>

            <table class="table table-bordered table-striped table-hover category-table">
                <thead class="thead-dark">
                    <tr>
                        <th class="textCenter">
                            <input id="selectAllImportados" type="checkbox">
                        </th>
                        <th class="textCenter">
                            Nome:
                        </th>
                        <th class="textCenter">
                            Sobrenome:
                        </th>
                        <th class="textCenter">
                            telefone:
                        </th>
                        <th class="textCenter">
                            email:
                        </th>
                    </tr>
                </thead>
                @foreach($contatos as $contact)
                <tbody class="background-contatos">
                    <tr class="tabela">
                        <td>
                            <input class="check" type="checkbox" onfocus="checkbox();">
                        </td>
                        <td class="textCenter">
                            <input type="text" class="yourName col-12 rounded" name="Nome[]" value="{{ $contact['name'] }}" disabled>
                        </td>
                        <td class="textCenter">
                            <input type="text" class="yourLastName col-12 rounded" name="SobreNome[]" value=" {{$contact['lastname']}}" disabled>
                        </td>
                        <td class="textCenter">

                            <input type="text" class="yourTel rounded" name="Tel[]" value=" {{$contact['tel']}}" disabled>

                        </td>
                        <td class="textCenter">
                            <input class=" yourEmail col-12 rounded" name="Email[]" type="text" value="{{$contact['email']}}" disabled>
                        </td>
                    </tr>

                </tbody>
                @endforeach

            </table>
        </form>
    </div>
</div>




<script>
    $(document).on('click', '#btnAddContatos', function(e) {

    });
</script>

<script>
    function checkbox() {
        enable_cb();
        $(".check").click(enable_cb);
    };

    function enable_cb() {

        var nameInput = $(this).closest('tr.tabela').find('input.yourName');
        var lastName = $(this).closest('tr.tabela').find('input.yourLastName');
        var tel = $(this).closest('tr.tabela').find('input.yourTel');
        var email = $(this).closest('tr.tabela').find('input.yourEmail');

        if ($(this).prop('checked')) {
            nameInput.removeAttr("disabled");
            lastName.removeAttr("disabled");
            tel.removeAttr("disabled");
            email.removeAttr("disabled");

        } else {
            nameInput.attr("disabled", true);
            lastName.attr("disabled", true);
            tel.attr("disabled", true);
            email.attr("disabled", true);
        }
    };
</script>
<script>
    $('button.btnTroca').on('click', function(e) {
        e.preventDefault();
        console.log('teste');   
        document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://agendaeduardo.com/contact/import/google"; 
        console.log('a');  
    });
</script>
<script>
    $('button.btnCancelar').on('click', function(e) {
        e.preventDefault();
        console.log('teste');   
        document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://agendaeduardo.com/contatos"; 
        console.log('a');  
    });
</script>


@stop