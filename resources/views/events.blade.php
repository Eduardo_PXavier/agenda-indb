@extends('layouts.app')

@section('content')


<div class="container">
    <div class="card card-primary">
        <div class="card-heading">
            <h1>
                Calendário
            </h1>
        </div>
        <!-- add event -->
        <div class="card-body">

            <div class="row">
                <div class="col-2">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-id="" data-target="#addModal">Adicionar Evento</button>
                </div>
                <div class="col-2">
                    <!-- <a href="http://127.0.0.1:8000/events/list" type="button" class="btn btn-warning"></a>                   -->
                    <button type="button" class="btn btn-warning list-event" id="list-event">Listar Eventos</button>
                </div>
            </div>

        </div>

    </div>
    <div class="card card-primary">
        <div class="card-heading">Detalhes de eventos</div>
        <div class="card-body">
            {!! $calendar_details->calendar()!!}
        </div>
    </div>

</div>
<div class="modal fade col-12" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 200px;max-width: 95%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar Evento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="event_name"> Nome do Evento:</label>
                                <!-- {!!Form::label('event_name', 'Nome do Evento:')!!} -->
                                <form name="form" id="register_form">
                                    <div>
                                        {!!Form::text('event_name', null, ['class' => 'form-control', 'placeholder'=>'Evento']) !!}
                                        {!! $errors->first('event_name', '<p class="alert alert-danger">:message</p>')!!}
                                    </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-group">
                                <label for="start_date"> Data Início</label>
                                <!-- {!!Form::label('start_date', 'Data de início:')!!} -->
                                <!-- 2019/08/25 12:20 -->
                                <div>
                                    <!-- <input type="datetime-local" class="form-control"  name="start_date"> -->
                                    {!!Form::input('date','start_date', null, ['class' => 'form-control','id' => 'startDate']) !!}
                                    {!!Form::input('time','start_time', '00:00', ['class' => 'form-control']) !!}
                                    {!! $errors->first('start_date', '<p class="alert alert-danger">:message</p>')!!}
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-group">
                                <label for="end_date"> Data Final:</label>
                                <!-- {!!Form::label('end_date', 'data do Término:')!!} -->
                                <div>
                                    {!!Form::input('date','end_date', null, ['class' => 'form-control', 'id' => 'endDate']) !!}
                                    {!!Form::input('time','end_time', '23:59', ['class' => 'form-control']) !!}
                                    <!-- Form::time('time', \Carbon\Carbon::now()->timezone('Europe/Brussels')->format('H:i:s'),['class' => 'form-control']) -->
                                    {!! $errors->first('end_date', '<p class="alert alert-danger">:message</p>')!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <!-- <button class="btn btn-danger delete-contact" value="">Deletar</button>  -->
                <!-- !! Form::submit('Adicionar Evento',['class'=>'btn btn-primary']) !! -->
                <button type="button" class="btn btn-primary add-event" value="" id="add-event">Adicionar Evento</button>
            </div>
            </form>

        </div>
    </div>

</div>
<script>
    @yield('pageScript')
</script>


{!!$calendar_details->script()!!}
<!-- <script src="http://code.jquery.com/jquery.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>


<script>
    $('.list-event').click(function() {
        window.location.href = ("http://agendaeduardo.com/events/list");
    });
</script>


<script>
    $('.add-event').click(function() {
        var event_name = $('input[name = event_name]').val();
        var start_date = $('input[name = start_date]').val() + ' ' + $('input[name = start_time]').val();
        var end_date = $('input[name = end_date]').val() + ' ' + $('input[name = end_time]').val();
        console.log(event_name);
        console.log(start_date);
        console.log(end_date);

        $.ajax({
            type: "POST",
            url: '/events',
            data: {
                _method: 'post',
                event_name,
                start_date,
                end_date,
                _token: "{{ csrf_token() }}"
            },
            success: function(response) {              
                alert(response['menssage']);
                location.reload();
            },
        });
    });
</script>
<script>
    $(document).ready(function() {
        var date = new Date();

        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var today = year + "-" + month + "-" + day;


        document.getElementById('startDate').value = today;
        document.getElementById('endDate').value = today;
    });
</script>
@endsection

<!-- fc-today -->