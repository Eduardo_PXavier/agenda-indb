@extends('layouts.app')

@section('content')
<div class="loggin--wrapper">
<div class="paginaForm__logging">
  <div class="container container__auth ">
    @include('flash')
    <form method="POST" action="/entrar" class="form-signin rounded" novalidate>
      <h2 class="form-signin-heading">Entrar</h2>

      {{ csrf_field() }}
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupPrepend">@</span>
          </div>
          <label for="inputEmail" class="sr-only">Email address</label>
          <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email" required>
          <div class="invalid-feedback">
            Email é necessário!
          </div>
        </div>
      </div>

      <div class="form-group form-group">
        <label for="inputPassword" class="sr-only">Password</label>
        <input name="password" type="password" class="form-control" id="passsword" placeholder="Senha" required>
        <div class="invalid-feedback">
          Senha é necessário!
        </div>
      </div>

      <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
    </form>
  </div>
  </div>
</div>
@if (count($errors) >0)
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<script>
  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('form-signin');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
@stop