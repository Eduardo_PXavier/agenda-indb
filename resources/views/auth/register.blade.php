@extends('layouts.app')

@section('content')

<div class="register--wrapper">
@if (count($errors) >0)
      <div class="alert alert-danger nobtomMar">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
  <div class="paginaForm__cadastro">

    <div class="container container__auth">
     
      <form method="POST" action="/cadastrar" class="form-signin rounded" novalidate>
        <div class="row">
          <div class="col-12">
            <h1>Cadastro</h1>
            {!! csrf_field() !!}

            <div class="form-group">
              <label for="name" class="sr-only"></label>
              <input type="text" name="name" id="name" class="form-control" placeholder="Nome*" value="{{old('name') }}" required>
              <div class="invalid-feedback">
                Nome é necessário!
              </div>
            </div>

            <div class="form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupPrepend">@</span>
                </div>
                <label for="email" class="sr-only"></label>
                <input type="text" name="email" id="email" class="form-control rounded-right" placeholder="Email*" value="{{old('email') }}" required>
                <div class="invalid-feedback">
                  Email é necessário!
                </div>
              </div>
            </div>

            <div class="form-group" class="sr-only">
              <label for="password" class="sr-only"></label>
              <input type="password" name="password" id="password" class="form-control" placeholder="Senha*" required>
              <div class="invalid-feedback">
                Adicione uma Senha com no minímo 6 caractéres.
              </div>
            </div>

            <div class="form-group" class="sr-only">
              <label for="password_confimartion" class="sr-only"></label>
              <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirmar Senha*" required>
              <div class="invalid-feedback">
                a senha e a confirmação tem que ser iguais.
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group form-group--no-bottom">
                  <a href="terms"> Termos e Condições </a>
                </div>
              </div>
            </div>
            <br />
            <div class="row">
              <div class="col-md-12">
                <div class="form-group form-group--no-bottom ">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                    <label class="form-check-label" for="invalidCheck">
                      Agree to terms and conditions
                    </label>
                    <div class="invalid-feedback">
                      You must agree before submitting.
                    </div>
                  </div>
                </div>
              </div>
            </div>



            <div class="form-group">
              <button class="btn btn-lg btn-primary btn-block" type="submit">Registrar</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>

</div>
<script>
  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('form-signin');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
@stop