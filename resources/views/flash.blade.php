@if(session()->has('message'))
    <div class="alert alert-dark alert-dismissible fade show rounded-0" role="alert">
        {{ session('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(session()->has('messageDanger'))
    <div class="alert alert-danger alert-dismissible fade show rounded-0" role="alert">
        {{ session('messageDanger') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

