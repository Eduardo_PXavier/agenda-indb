@extends('mobile.app')

@section('content')


<div class="container-fluid ">
  <div class="row ">
    <div class="col-12 definicao-pagina">
      <!-- method="POST" action="/new-contato"  -->
      <!-- <form class="contato-needs-validation rounded" method="PATCH" action="contato/{{$contact->id}}" novalidate> -->
      {!! Form::open(['route' => ['contato.update', $contact->id ],'id'=>'form','method' => 'PATCH', 'class' => 'contato-needs-validation rounded', 'onsubmit'=>'return validaCPF()']) !!}
      {!! csrf_field() !!}
      @include('flash')
      <div>
        <h4>Editando: {{ $contact->name }}</h4>
      </div>
      <!-- nome e sobrenome -->
      <div class="form-row">
        <!-- nome -->
        <div class="col-md-12">
          <div class="form-group">
            <label for="name">Nome:</label>
            <input type="text" class="form-control" id="name" placeholder="Nome" value="{{$contact->name}}" name="name" required>
            <div class="valid-feedback">
              Válido!
            </div>
            <div class="invalid-feedback">
              Nome é necessário!
            </div>
          </div>
        </div>
        <!-- sobre-nome -->
        <div class="col-md-12">
          <div class="form-group">
            <label for="last_name">Sobre Nome:</label>
            <input type="text" class="form-control" id="last_name" placeholder="Sobre Nome" name="last_name" value="{{$contact->last_name}}" required>
            <div class="valid-feedback">
              Válido!
            </div>
            <div class="invalid-feedback">
              Sobre Nome é necessário!
            </div>
          </div>
        </div>
      </div>

      <!-- telefon cpf e categoria -->
      <div class="form-row" data-id="{{$contact->id}}" id="idParaContato" style="margin-top: 15px;">
        <!-- telefone -->
        <div class="col-12">
          <!-- verifica se tem telefone no banco -->
          @if(!empty($contact->tels->all()))

          <label for="telefone">Telefone:</label>
          @foreach( $contact->tels as $tel )
          <div class="form-group nobtomMar">
            <div id="divTel{{$tel->id}}" class="input-group">
              <input type="text" class="form-control form-group multiple-form-group masktel" id="telefone" placeholder="Telefone 1" name="tel[{{$tel->id}}]" value="{{ $tel->tel }}" required>
              @if($tel==$contact->tels->first())
              <span class="input-group-btn rounded-right">
                <button type="button" class="btn btnAddTel btn-right btn-success rounded-right">+</button>
              </span>
              @else
              <span class="input-group-btn rounded-right">
                <button type="button" data-id="{{$tel->id}}" class="btn btnDeleteTel btn-right btn-danger  btn-remove rounded-right">-</button>
              </span>
              @endif

            </div>
            <div class="valid-feedback">
              Válido!
            </div>
            <div class="invalid-feedback">
              Telefone é necessário!
            </div>
          </div>
          @endforeach
          <!-- Novo telefone entra aqui -->
          <div id="telefone1">
            <div class="container tel teste" id="teste" style="padding: 0px;" hidden>
              <div class="input-group">
                <input type="text" class="form-group multiple-form-group form-control tel rounded-left masktel" id="telefone" placeholder="Telefone" name="newTel[]">
                <span class="input-group-btn rounded-right">
                  <button type="button" class="btn  btn-danger btn-remove  btn-right rounded-right">-</button>
                </span>
              </div>
            </div>
          </div>

          @else
          <div class="container no-padding">
            <div class="col-12">
              <div class="form-group nobtomMar">
                <label for="telefone">Telefone:</label>
                <div class="input-group">
                  <input type="text" class="form-control" id="telefone" placeholder="Telefone 1" name="tel[]" value="" required>
                  <span class="input-group-btn rounded-right">
                    <button type="button" class="btn btnAddTel btn-right btn-success rounded-right">+</button>
                  </span>
                  <div class="valid-feedback">
                    Válido!
                  </div>
                  <div class="invalid-feedback">
                    Telefone é necessário!
                  </div>
                </div>
              </div>
            </div>

            <!-- teste -->
            <div id="telefone1">
              <div class="container tel teste nobtomMar" id="teste" style="padding: 0px;" hidden>
                <div class="input-group">
                  <input type="text" class="form-group multiple-form-group form-control tel rounded-left masktel" id="telefone" placeholder="Telefone" name="newTel[]">
                  <span class="input-group-btn rounded-right">
                    <button type="button" class="btn  btn-danger btn-remove  btn-right rounded-right">-</button>
                  </span>
                </div>
              </div>
            </div>
          </div>
          @endif
        </div>

        <!-- categorias -->
        @if(!empty($contact->categories->all()))
        <div class="col-md-12 mb-3">
          <label for="category">Categoria:</label>
          @foreach( $contact->categories as $category )
          <div class="form-group nobtomMar">
            <div id="divCategory{{$category->id}}" class="input-group">
              <input type="text" class="form-control" id="category" name="category[{{$category->id}}]" placeholder="Categoria" value="{{$category->category}}">
              @if($category==$contact->categories->first())
              <span class="input-group-btn rounded-right">
                <button type="button" class="btn btnAddCategory btn-right btn-success rounded-right">+</button>
              </span>
              @else
              <span class="input-group-btn rounded-right">
                <button type="button" data-id="{{$category->id}}" class="btn btnDeleteCategory btn-right btn-danger  btn-remove rounded-right">-</button>
              </span>
              @endif
            </div>
          </div>
          @endforeach
          <!-- nova categoria -->
          <div id="category1">
            <div class="container newCategory" style="padding: 0px;" hidden>
              <div class="input-group">
                <input type="text" class="form-group multiple-form-group form-control rounded-left" id="category" placeholder="Categoria" name="newCategory[]">
                <span class="input-group-btn rounded-right">
                  <button type="button" data-id="{{$category->id}}" class="btn btnDelete btn-right btn-danger  btn-remove rounded-right">-</button>
                </span>
              </div>
            </div>
          </div>
        </div>
        @else
        <div class="col-md-12 mb-3">
          <label for="category">Categoria:</label>
          <div class="input-group">
            <input type="text" class="form-control" id="category" name="category[]" placeholder="Categoria" value="">
            <span class="input-group-btn rounded-right">
              <button type="button" class="btn btnAddCategory btn-right btn-success rounded-right">+</button>
            </span>
          </div>
          <div id="category1">
            <div class="container newCategory" style="padding: 0px;" hidden>
              <div class="input-group">
                <input type="text" class="form-group multiple-form-group form-control rounded-left" id="category" placeholder="Categoria" name="newCategory[]">
                <span class="input-group-btn rounded-right">
                  <button type="button" class="btn btnDelete btn-right btn-danger  btn-remove rounded-right">-</button>
                </span>
              </div>
            </div>
          </div>
        </div>
        @endif
        <!-- cpf -->
        @if(!empty($contact->cpfs->all()))
        <div class="col-md-12 mb-3">
          <label for="cpf">CPF:</label>
          @foreach( $contact->cpfs as $cpf )
          <input type="text" class="form-control" id="cpf" name="cpf[{{$cpf->id}}]" placeholder="CPF" value="{{$cpf->cpf}}">
          @endforeach
        </div>
        @else
        <div class="col-md-12 mb-3">
          <label for="cpf">CPF:</label>
          <input type="text" class="form-control" id="cpf" name="cpf[]" placeholder="CPF" value="">
        </div>
        @endif


      </div>
      <!-- email -->
      <div class="form-row">

        <!-- email -->
        <div class="form-row col-12">
          <!-- verfica se tem ou nao email -->
          @if(!empty($contact->emails->all()))
          
            <label for="email">Email:</label>
            <!-- emails do banco -->
            @foreach( $contact->emails as $email )
            <div class="form-group nobtomMar">
              <div id="divEmail{{$email->id}}" class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupPrepend">@</span>
                </div>
                <input type="email" class="form-control rounded-0 " id="email" name="email[{{$email->id}}]" placeholder="Email" value="{{ $email->email }}" aria-describedby="inputGroupPrepend">
                @if($email==$contact->emails->first())
                <span class="input-group-btn rounded-right">
                  <button type="button" class="btn btnAddEmail btn-right btn-success rounded-right">+</button>
                </span>
                @else
                <span class="input-group-btn rounded-right">
                  <button type="button" data-id="{{$email->id}}" class="btn btnDeleteEmail btn-right btn-danger btn-remove rounded-right">-</button>
                </span>
                @endif
              </div>
            </div>
            @endforeach
            <!-- Novo email entra aqui -->
            <div id="email1">
              <!-- container hiden-->
              <div class="container newEmail" style="padding: 0px;" hidden>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                  </div>
                  <input type="text" class="form-group multiple-form-group form-control rounded-0" id="email" placeholder="Email" name="newEmail[]">
                  <span class="input-group-btn rounded-right">
                    <button type="button" data-id="{{$email->id}}" class="btn btnDelete btn-right btn-danger btn-remove rounded-right">-</button>
                  </span>
                </div>
              </div>
            </div>
          
          @else
          <div class="container" style="padding-left: 5px;">
            <div class="col-md-12" style="padding: 0px;">
              <div class="form-group" style="margin-bottom: 0px;">
                <label for="email">Email:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                  </div>
                  <input type="email" class="form-control rounded-0" id="email" name="email[]" placeholder="Email" value="" aria-describedby="inputGroupPrepend">
                  <span class="input-group-btn rounded-right">
                    <button type="button" class="btn btnAddEmail btn-right btn-success rounded-right">+</button>
                  </span>

                </div>
              </div>
            </div>
            <!-- teste -->
            <div id="email1">
              <!-- container hiden-->
              <div class="container newEmail" style="padding: 0px;" hidden>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                  </div>
                  <input type="text" class="form-group multiple-form-group form-control rounded-0" id="emailefone" placeholder="Email" name="newEmail[]">
                  <span class="input-group-btn rounded-right">
                    <button type="button" class="btn btnDelete btn-right btn-danger btn-remove rounded-right">
                      -
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </div>
          @endif
        </div>


      </div>




      <!-- endereço -->
      <div class="form-row" style="margin-top: 20px;">
        <!-- || !is_null($contact->addresses->all() -->
        @if(!empty($contact->addresses->all()))
        <label>Endereço:</label>
        <div class="no-padding col-md-12 text-center">
          <span class="input-group-btn rounded-right">
            <button type="button" class="btn btn-success btnAddAddress rounded-right">Novo Endereço</button>
          </span>
        </div>
        @foreach($contact->addresses as $address)
        <div id="divAddress{{$address->id}}" class="form-row">
          @if(!($address==$contact->addresses->first()))
          <div class="no-padding col-md-12 text-center">
            <span class=" rounded-right">
              <button type="button" data-id="{{$address->id}}" class="btn btnDeleteAddress btn-danger btn-remove rounded-right">-</button>
            </span>
          </div>
          @endif
          <div class="col-md-3 mb-3">
            <input type="text" class="form-control cep" id="cep" onblur="pesquisacep(this, this.value);" name="cep[{{$address->id}}]" placeholder="CEP" value="{{ $address->cep }}" onkeypress="$(this).mask('00000000')">
          </div>
          <div class="col-md-6 mb-3">
            <input type="text" class="form-control" id="city" name="city[{{$address->id}}]" value="{{ $address->city }}" placeholder="Cidade">
          </div>
          <div class="col-md-3 mb-3">
            <input type="text" class="form-control" id="state" name="state[{{$address->id}}]" placeholder="Estado" value="{{ $address->state }}">
          </div>
          <div class="col-md-12 mb-3">
            <input type="text" class="form-control" id="street" name="street[{{$address->id}}]" placeholder="Rua" value="{{ $address->street }}">
          </div>
          <div class="col-md-6 mb-3">
            <input type="text" class="form-control" id="neighborhood" name="neighborhood[{{$address->id}}]" placeholder="Bairro" value="{{ $address->neighborhood }}">
          </div>
          <div class="col-md-3 mb-3">
            <input type="text" class="form-control" id="number" name="number[{{$address->id}}]" placeholder="Número" value="{{ $address->number }}">
          </div>
        </div>
        @endforeach
        <!-- novo endereço -->
        <div id="address1">
          <div class="container newAddress col-12" style="padding: 0px; margin: 0px;" hidden>
            <div class="input-group" id=novoEnd>
              <div class="no-padding col-md-12 text-center">
                <span class=" rounded-right">
                  <button type="button" class="btn btn-danger btn-remove rounded-right">-</button>
                </span>
              </div>
              <div class="form-row">
                <div class="col-md-3 mb-3">
                  <input type="text" class="form-control cep" id="cep" onblur="pesquisacep(this, this.value);" name="newCep[]" placeholder="CEP" value="" onkeypress="$(this).mask('00000000')">
                </div>
                <div class="col-md-6 mb-3">
                  <input type="text" class="form-control city" name="newCity[]" value="" placeholder="Cidade">
                </div>
                <div class="col-md-3 mb-3">
                  <input type="text" class="form-control state" id="state" name="newState[]" placeholder="Estado" value="">
                </div>
                <div class="col-md-12 mb-3">
                  <input type="text" class="form-control street" id="street" name="newStreet[]" placeholder="Rua" value="">
                </div>
                <div class="col-md-6 mb-3">
                  <input type="text" class="form-control neighborhood" id="neighborhood" name="newNeighborhood[]" placeholder="Bairro" value="">
                </div>
                <div class="col-md-3 mb-3">
                  <input type="text" class="form-control number" id="number" name="newNumber[]" placeholder="Número" value="">
                </div>
              </div>
            </div>
          </div>
        </div>
        @else
        <div class="form-row">
          <label class="col-md-12">Endereço:</label>
          <div class="no-padding col-md-12 text-center">
            <span class="input-group-btn rounded-right">
              <button type="button" class="btn btn-success btnAddAddress rounded-right">Novo Endereço</button>
            </span>
          </div>
          <div class="col-md-3 mb-3">
            <input type="text" class="form-control cep" id="cep" onblur="pesquisacep(this, this.value);" name="newCep[]" placeholder="CEP" value="" onkeypress="$(this).mask('00000000')">
          </div>
          <div class="col-md-6 mb-3">
            <input type="text" class="form-control city" id="city" name="newCity[]" value="" placeholder="Cidade">
          </div>
          <div class="col-md-3 mb-3">
            <input type="text" class="form-control" id="state" name="newState[]" placeholder="Estado" value="">
          </div>
          <div class="col-md-12 mb-3">
            <input type="text" class="form-control" id="street" name="newStreet[]" placeholder="Rua" value="">
          </div>
          <div class="col-md-6 mb-3">
            <input type="text" class="form-control" id="neighborhood" name="newNeighborhood[]" placeholder="Bairro" value="">
          </div>
          <div class="col-md-3 mb-3">
            <input type="text" class="form-control" id="number" name="newNumber[]" placeholder="Número" value="">
          </div>
          <input type="text" class="form-control" name="cep[]" hidden>
          <input type="text" class="form-control" name="city[]" hidden>
          <input type="text" class="form-control" name="state[]" hidden>
          <input type="text" class="form-control" name="street[]" hidden>
          <input type="text" class="form-control" name="neighborhood[]" hidden>
          <input type="text" class="form-control" name="number[]" hidden>
        </div>
        <div id="address1">
          <div class="container newAddress col-12" style="padding: 0px; margin: 0px;" hidden>
            <div class="input-group" id=novoEnd data-id="">
              <div class="no-padding col-md-12 text-center">
                <span class=" rounded-right">
                  <button type="button" class="btn  btn-danger btn-remove rounded-right">-</button>
                </span>
              </div>
              <div class="form-row">
                <div class="col-md-3 mb-3">
                  <input type="text" class="form-control cep" id="cep" onblur="pesquisacep(this, this.value);" name="newCep[]" placeholder="CEP" value="" onkeypress="$(this).mask('00000000')">
                </div>
                <div class="col-md-6 mb-3">
                  <input type="text" class="form-control city" id="city" name="newCity[]" value="" placeholder="Cidade">
                </div>
                <div class="col-md-3 mb-3">
                  <input type="text" class="form-control state" id="state" name="newState[]" placeholder="Estado" value="">
                </div>
                <div class="col-md-12 mb-3">
                  <input type="text" class="form-control street" id="street" name="newStreet[]" placeholder="Rua" value="">
                </div>
                <div class="col-md-6 mb-3">
                  <input type="text" class="form-control neighborhood" id="neighborhood" name="newNeighborhood[]" placeholder="Bairro" value="">
                </div>
                <div class="col-md-3 mb-3">
                  <input type="text" class="form-control number" id="number" name="newNumber[]" placeholder="Número" value="">
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif
      </div>
      <div class="row">
        <div class="col-12">
          <div class="text-center">
            <button class="btn btn-primary" type="submit">Salvar Edição</button>
            <button type="button" data-id='{{$contact->id}}' class="btn btnCancel btn-danger-tabela">Cancelar Edição</button>
          </div>
        </div>
      </div>

      {!! Form::close() !!}
    </div>
  </div>
</div>
@if (count($errors) >0)
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<script>
  (function($) {
    $(function() {

      var addFormGroup = function(event) {
        event.preventDefault();

        var $formGroup = $(this).closest('.clone');
        var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
        var $formGroupClone = $formGroup.clone();

        $(this)
          .toggleClass('btn-success btn-add btn-danger btn-remove')
          .html('–');

        $formGroupClone.insertBefore($formGroup);

        var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
        if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
          $lastFormGroupLast.find('.btn-add').attr('disabled', true);
        }
      };

      var removeFormGroup = function(event) {
        event.preventDefault();

        var $Container = $(this).closest('.container');
        var $multipleFormGroup = $Container.closest('.multiple-form-group');

        var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
        if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
          $lastFormGroupLast.find('.btn-add').attr('disabled', false);
        }

        $Container.remove();
      };


      var countFormGroup = function($form) {
        return $form.find('.form-group').length;
      };

      $(document).on('click', '.btn-add', addFormGroup);
      $(document).on('click', '.btn-remove', removeFormGroup);


    });
  })(jQuery);
</script>

<script>
  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('contato-needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            $('.masktel').mask('(00) 00 0000-0000');
            $('.maskcpf').mask('000.000.000-00');
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
</script>
<!-- clona tel -->
<script>
  $('button.btnAddTel').on('click', function(e) {
    e.preventDefault();
    console.log('teste');
    var input = document.getElementsByClassName("tel")[0];
    console.log(input);
    var add = document.getElementById("telefone1");
    var clone = input.cloneNode(true);
    clone.style.display = 'block';
    clone.hidden = false;
    const elementAdded = add.parentElement.appendChild(clone);
    $('.masktel').mask('(00) 00 0000-0000');

  });
</script>
<!-- clona email -->
<script>
  $('button.btnAddEmail').on('click', function(e) {
    e.preventDefault();
    console.log('teste');
    var input = document.getElementsByClassName("newEmail")[0];
    console.log(input);
    var add = document.getElementById("email1");
    var clone = input.cloneNode(true);
    clone.style.display = 'block';
    clone.hidden = false;
    clone.value = "";
    add.parentElement.appendChild(clone);
  });
</script>
<!-- clona categoria -->
<script>
  $('button.btnAddCategory').on('click', function(e) {
    e.preventDefault();
    console.log('teste');
    var input = document.getElementsByClassName("newCategory")[0];
    console.log(input);
    var add = document.getElementById("category1");
    var clone = input.cloneNode(true);
    clone.style.display = 'block';
    clone.hidden = false;
    clone.value = "";
    add.parentElement.appendChild(clone);
  });
</script>
<!-- clona endereço -->
<script>
  var count = 1;
  $('button.btnAddAddress').on('click', function(e) {
    e.preventDefault();

    var novoEnd = $('#novoEnd');

    console.log(novoEnd);
    novoEnd.find('input.city').attr('id', "city" + count);
    novoEnd.find('input.state').attr('id', "state" + count);
    novoEnd.find('input.cep').attr('id', "cep" + count).attr('data-id', count);
    novoEnd.find('input.street').attr('id', "street" + count);
    novoEnd.find('input.neighborhood').attr('id', "neighborhood" + count);
    novoEnd.find('input.number').attr('id', "number" + count);

    count++;
    var input = document.getElementsByClassName("newAddress")[0];
    console.log(input);
    var add = document.getElementById("address1");
    var clone = input.cloneNode(true);
    clone.style.display = 'block';
    clone.hidden = false;
    add.parentElement.appendChild(clone);
  });
</script>

<!-- preenchimento por cep -->
<script type="text/javascript">
  //Foca no Cep
  function pesquisacep(input, value) {
    // var idEnd = valor.closest('input').data('id');

    console.log(input);
    var endereco = input.closest('div.form-row');
    console.log(endereco);
    var cep = value;
    console.log(cep);
    ajax = new XMLHttpRequest();

    var url = "http://viacep.com.br/ws/" + cep + "/json/";
    ajax.open('GET', url, true);
    ajax.send();

    ajax.onreadystatechange = function() {
      if (ajax.readyState == 4 && ajax.status == 200) {

        var json = JSON.parse(ajax.responseText);
        endereco.getElementsByTagName('input')[0].value = cep;
        endereco.getElementsByTagName('input')[1].value = json.localidade;
        endereco.getElementsByTagName('input')[2].value = json.uf;
        endereco.getElementsByTagName('input')[3].value = json.logradouro;
        endereco.getElementsByTagName('input')[4].value = json.bairro;
        endereco.getElementsByTagName('input')[5].focus();

      }
    }
  }
</script>

<script>
  $('button.btnCancel').on('click', function(e) {
    e.preventDefault();
    console.log('teste');
    var contatoId = $(this).data('id');
    console.log(contatoId);
    $.ajax({
      type: "POST",
      url: '/restoreAll',
      data: {
        id: contatoId,
        _token: "{{ csrf_token() }}"
      },
      success: function(data) {
        window.location.href = 'http://m.agendaeduardo.com/contatos';
      }

    });

  });
</script>
<!-- delete Tel existente -->
<script>
  $('button.btnDeleteTel').on('click', function(e) {
    e.preventDefault();
    console.log('teste');
    var id = $(this).data('id');
    console.log(id);
    $.ajax({
      type: "POST",
      url: '/delete/tel/' + id,
      data: {
        _method: 'delete',
        _token: "{{ csrf_token() }}"
      },
      success: function(data) {

        $('button.btnDeleteTel').closest('#divTel' + id).remove();

      }
    });
  });
</script>
<!-- delete email -->
<script>
  $('button.btnDeleteEmail').on('click', function(e) {
    e.preventDefault();
    console.log('teste');
    var id = $(this).data('id');
    console.log(id);
    $.ajax({
      type: "POST",
      url: '/delete/email/' + id,
      data: {
        _method: 'delete',
        _token: "{{ csrf_token() }}"
      },
      success: function(data) {
        $('button.btnDeleteEmail').closest('#divEmail' + id).remove();
      }
    });
  });
</script>
<!-- delete categoria -->
<script>
  $('button.btnDeleteCategory').on('click', function(e) {
    e.preventDefault();
    console.log('teste');
    var id = $(this).data('id');
    var contatoId = $('#idParaContato').data('id');
    console.log(id);
    $.ajax({
      type: "POST",
      url: '/delete/categoria/' + id + '/' + contatoId,
      data: {
        _method: 'delete',
        _token: "{{ csrf_token() }}"
      },
      success: function(data) {
        $('button.btnDeleteCategory').closest('#divCategory' + id).remove();
        console.log(id);
      }
    });
  });
</script>
<script>
  $('button.btnDeleteAddress').on('click', function(e) {
    e.preventDefault();
    console.log('teste');
    var id = $(this).data('id');
    console.log(id);
    $.ajax({
      type: "POST",
      url: '/delete/endereco/' + id,
      data: {
        _method: 'delete',
        _token: "{{ csrf_token() }}"
      },
      success: function(data) {
        $('button.btnDeleteAddress').closest('#divAddress' + id).remove();
      }
    });
  });
</script>
<!-- mascara -->
<script>
  $(document).ready(function() {
    $('#telefone').mask('(00) 00 0000-0000');
    $('.masktel').mask('(00) 00 0000-0000');
    $('#cpf').mask('000.000.000-00');
  });
  $("#form").submit(function() {
    $('#telefone').unmask();
    $('.masktel').unmask();
    $('#cpf').unmask();

  })
  // onkeypress="$(this).mask('(00) 0000-0000')"
</script>
<!-- valida cpf -->
<script type="text/javascript">
  function validaCPF() {
    $('#cpf').unmask();
    cpf = document.getElementById("cpf").value;
    console.log(cpf);
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    console.log(cpf);
    if (cpf != "") {
      console.log('cpf');
      if (cpf.length < 11) {

        alert('CPF INVÁLIDO');
        return false;
      }
      for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1)) {
          digitos_iguais = 0;
          break;
        }
      if (!digitos_iguais) {
        numeros = cpf.substring(0, 9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--) {
          soma += numeros.charAt(10 - i) * i;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0)) {

          alert('CPF INVÁLIDO');
          return false;
        }
        numeros = cpf.substring(0, 10);
        soma = 0;
        for (i = 11; i > 1; i--) {
          soma += numeros.charAt(11 - i) * i;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1)) {

          alert('CPF INVÁLIDO');
          return false;
        }
        return true;
      } else {

        alert('CPF INVÁLIDO');
        return false;
      }
    }
  }
</script>
@stop