@extends('mobile.app')


@section('content')

<div class="col-12">
  <div class="row">
    <div class="col-12">
      {!! csrf_field() !!}
      @include('flash')
      <h1 class="rounded-top" style="background: #504f4f54;padding: 
                    2px;margin-bottom: 0px;margin-top: 10px;color: #000;">
        Resultados de pesquisa para: #{{$search}}
      </h1>



      <div class="container col-12" style="background-color: #7974bbb3; padding-bottom: 1vh;">
        <div class="textCenter col-12">
          <h3>Valor:</h3>
        </div>
        <div class="textCenter col-12">

          <div class="dropdown col-12">
            <a class="btn btn-info dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="divue" aria-expanded="false">
              Tipo
            </a>

            <div class="dropdown-menu col-2" aria-labelledby="dropdownMenuLink">
              <div class="dropdown-item">
                <input id="selectAll" type="checkbox" checked> TODOS
              </div>
              <div class="dropdown-item">
                <input class="check" type="checkbox" id="checkContato" onfocus="checkbox();" checked> CONTATO
              </div>
              <div class="dropdown-item">
                <input class="check" type="checkbox" id="checkEmail" onfocus="checkbox();" checked> EMAIL
              </div>
              <div class="dropdown-item">
                <input class="check" type="checkbox" id="checkTelefone" onfocus="checkbox();" checked> TELEFONE
              </div>
              <div class="dropdown-item">
                <input class="check" type="checkbox" id="checkCpf" onfocus="checkbox();" checked> CPF
              </div>
              <div class="dropdown-item">
                <input class="check" type="checkbox" id="checkEndereco" onfocus="checkbox();" checked> ENDEREÇO
              </div>
              <div class="dropdown-item">
                <input class="check" type="checkbox" id="checkEvento" onfocus="checkbox();" checked> EVENTO
              </div>
              <div class="dropdown-item">
                <input class="check" type="checkbox" id="checkCategoria" onfocus="checkbox();" checked> CATEGORIA
              </div>
            </div>
          </div>
        </div>
      </div>

      @foreach($resultadoFinalContatos as $contact)
      <div class="background-contatos TabelaContato" style="margin-bottom: 1vh;">
        <div class="btnDelete" id="{{$contact->id}}" data-id="{{$contact->id}}">
          <div class="textCenter">
            <a href="{{route('contato.edit',$contact->id)}}">
              <p>{{ $contact->name }}
                <span style="font-size: 14px"> {{ $contact->last_name }}</span>
              </p>
            </a>
          </div>
          <div>
            CONTATO
          </div>
        </div>
      </div>
      @endforeach
      @foreach($resultadoFinalEmail as $email)
      <div class="background-emails TabelaEmail" style="margin-bottom: 1vh;">
        <div class="btnDelete" id="{{$email->id}}" data-id="{{$email->id}}">
          <div class="textCenter">
            <a href="{{route('contato.edit',$email->contato_id)}}">
              <p>
                {{ $email->email }}
              </p>
            </a>
          </div>
          <div>
            EMAIL
          </div>
        </div>
      </div>
      @endforeach
      @foreach($resultadoFinalTel as $tel)
      <div class="background-tels TabelaTel"style="margin-bottom: 1vh;">
        <div class="btnDelete" id="{{$tel->id}}" data-id="{{$tel->id}}">
          <div class="textCenter">
            <a href="{{route('contato.edit',$tel->contato_id)}}">
              <p>
                {{ $tel->tel }}
              </p>
            </a>
          </div>
          <div>
            TELEFONE
          </div>
        </div>
      </div>
      @endforeach
      @foreach($resultadoFinalCpf as $cpf)
      <div class="background-cpfs TabelaCpf" style="margin-bottom: 1vh;">
        <div class="btnDelete" id="{{$cpf->id}}" data-id="{{$cpf->id}}">
          <div class="textCenter">
            <a href="{{route('contato.edit',$cpf->contato_id)}}">
              <p>
                {{ $cpf->cpf }}
              </p>
            </a>
          </div>
          <div>
            CPF
          </div>
        </div>
      </div>
      @endforeach
      @foreach($resultadoFinalAddress as $address)
      <div class="background-endereco TabelaEndereco" style="margin-bottom: 1vh;">
        <div class="btnDelete" id="{{$address->id}}" data-id="{{$address->id}}">
          <div class="textCenter">
            <a href="{{route('contato.edit',$address->contato_id)}}">
              <p>
                {{ $address->city }}-{{$address->state}}- {{ $address->neighborhood }}- {{ $address->sdiveet }}- Nº:{{ $address->number }}- CEP: {{ $address->cep }}.
              </p>
            </a>
          </div>
          <div>
            ENDEREÇO
          </div>
        </div>
      </div>
      @endforeach
      @foreach($resultadoEventos as $event)
      <div class="background-evento TabelaEvento" style="margin-bottom: 1vh;">
        <div class="btnDelete" id="{{$event->id}}" data-id="{{$event->id}}">
          <div class="textCenter">
            <a href="{{route('events.edit',$event->id)}}">
              <p>
                Evento: {{ $event->event_name }}-> Início: {{ $event->start_date }}H -> Término {{$event->end_date}}H;
              </p>
            </a>
          </div>
          <div>
            EVENTO
          </div>
        </div>
      </div>
      @endforeach
      @foreach($resultadoCategories as $category)
      <div class="background-contato-cetegoria TabelaCategoria" style="margin-bottom: 1vh;">
        <div class="btnDelete" id="{{$category->id}}" data-id="{{$category->id}}">
          <div class="textCenter">
            <a href="{{route('pesquisaCategoria',$category->category)}}">
              <p>
                {{ $category->category }}
              </p>
            </a>
          </div>
          <div>
            CATEGORIA
          </div>
        </div>
      </div>
      @endforeach



    </div>
  </div>
</div>

<script>
  $('#selectAll').click(function() {

    if ($(this).prop('checked')) {
      $('.check').prop('checked', true);
      $('.background-contatos').show();
      $('.background-emails').show();
      $('.background-tels').show();
      $('.background-cpfs').show();
      $('.background-evento').show();
      $('.background-endereco').show();
      $('.background-contato-cetegoria').show();
    } else {
      $('.check').prop('checked', false);
      $('.background-contatos').hide();
      $('.background-emails').hide();
      $('.background-tels').hide();
      $('.background-cpfs').hide();
      $('.background-evento').hide();
      $('.background-endereco').hide();
      $('.background-contato-cetegoria').hide();
    }
  });
</script>
<script>
  $('#checkContato').click(function() {
    if ($(this).prop('checked')) {
      $('.TabelaContato').show();
    } else {
      $('.TabelaContato').hide();
    }
  });
</script>
<script>
  $('#checkEmail').click(function() {
    if ($(this).prop('checked')) {
      $('.TabelaEmail').show();
    } else {
      $('.TabelaEmail').hide();
    }
  });
</script>
<script>
  $('#checkTelefone').click(function() {
    if ($(this).prop('checked')) {
      $('.TabelaTel').show();
    } else {
      $('.TabelaTel').hide();
    }
  });
</script>
<script>
  $('#checkCpf').click(function() {
    if ($(this).prop('checked')) {
      $('.TabelaCpf').show();
    } else {
      $('.TabelaCpf').hide();
    }
  });
</script>
<script>
  $('#checkEndereco').click(function() {
    if ($(this).prop('checked')) {
      $('.TabelaEndereco').show();
    } else {
      $('.TabelaEndereco').hide();
    }
  });
</script>
<script>
  $('#checkEvento').click(function() {
    if ($(this).prop('checked')) {
      $('.TabelaEvento').show();
    } else {
      $('.TabelaEvento').hide();
    }
  });
</script>
<script>
  $('#checkCategoria').click(function() {
    if ($(this).prop('checked')) {
      $('.TabelaCategoria').show();
    } else {
      $('.TabelaCategoria').hide();
    }
  });
</script>

@stop