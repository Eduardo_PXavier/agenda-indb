@extends('mobile.app')


@section('content')

<div class="container-fluid">
  <div class="row">

    <div class="col-12 ">
      {!! csrf_field() !!}

      <div>

        <h1 class="rounded-top" style="background: #00000054;padding: 
                    2px;margin-bottom: 0px;margin-top: 10px;">
          Lixeira:
        </h1>



        @foreach($contacts as $contact)

        {!! csrf_field() !!}
        <div class="container col-12" data-id="{{$contact->id}}" style="margin-bottom: 1vh;background-color: #b5ef81e6;padding-bottom: 1vh;">
      
        <div>
          <p>{{ $contact->name }}
            <span style="font-size: 14px"> {{ $contact->last_name }}</span>
          </p>
        </div>
        <div>
          @if(is_null($contact->tels->first()))
          ----
          @else
          {{ $contact->tels->first()->tel}}
          @endif
        </div>
        <div>
          <button type="button" class="btn btn-primary btnRestore" data-toggle="modal" data-id="" data-target="#myModal">Restaurar</button>
          <button type="button" class="btn btn-danger-tabela btnDeleteMobile" data-toggle="modal" data-id="" data-target="#myModal2">Excluir</button>
        </div>
      
      </div>
      @endforeach
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="divue">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Restaurar?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="divue">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Tem certeza que deseja restaurar contato?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <form id="form-restore" action="" method="POST">
                {!! csrf_field() !!}

                <button type="submit" class="btn btn-primary restore-contact" id="btnResYes">Restaurar</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="divue">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Excluir?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="divue">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>
                Tem certeza que deseja excluir contato?<br>
                contato nao poderá ser recuperado.
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <form id="form-delete" action="" method="POST">
                {!! csrf_field() !!}
                <button type="submit" class="btn btn-danger restore-contact" id="btnResYes">Excluir</button>
              </form>
            </div>
          </div>
        </div>
      </div>

      </div>
    </div>
  </div>
</div>
</div>

<!-- <script>

    $('.restore-contact').click(function () {
        var id = $(this).val();
        console.log('teste');   
        $.ajax({
            type:"POST",
            url:'/restore/' + id,
            data:{_method: 'post', _token : "{{ csrf_token() }}"},
            success: function (data) {
                console.log(data);
                $("#"+id).remove();
                
                
            },
            error: function (data) {
                console.log('Error:', data);
            }

        });

    });
</script> -->

<script>
  $('button.btnRestore').on('click', function() {
    
    // console.log('teste');
    var id = $(this).closest('div.container').data('id');
    console.log(id);

    var idForm = $('#form-restore');
    console.log(idForm);
    $("#form-restore").attr("action", "restore/" + id);
    console.log(idForm);

  });
</script>
<script>
  $('button.btnDeleteMobile').on('click', function() {
    
    // console.log('teste');
    var id = $(this).closest('div.container').data('id');
    console.log(id);

    var idForm = $('#form-delete');

    $("#form-delete").attr("action", "exclui/" + id);
    console.log(idForm);

  });
</script>
@stop