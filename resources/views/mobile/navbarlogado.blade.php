
<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark ">
  <a class="navbar-brand" href="/contatos">Agenda</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/contato">Novo Contato <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/categoria">Categoria</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('lixeira')}}">Lixeira</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('events.index')}}">Calendário</a>
      </li>

      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ substr( Auth::user()->name , 0,3)}}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Perfil</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{route('logout')}}">Sair</a>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0 col-12" action="http://m.agendaeduardo.com/search?search=" method="get">
      <input class="form-control mr-sm-2 col-8" type="search" placeholder="Search"  name="search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0 col-3" type="submit">Search</button>
    </form>
  </div>
</nav>