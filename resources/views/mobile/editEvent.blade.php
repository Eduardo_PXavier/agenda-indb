@extends('mobile.app')

@section('content')


  {!! Form::open(['route' => ['events.update', $event->id ],'id'=>'form','method' => 'PATCH', 'class' => 'contato-needs-validation rounded']) !!}
  {!! csrf_field() !!}
  @include('flash')
  
    <div class="col-12">
      <div class="form-group">
        <label for="event_name"> Nome do Evento:</label>
        <!-- {!!Form::label('event_name', 'Nome do Evento:')!!} -->
        <form name="form" id="register_form">
          <div>
            {!!Form::text('event_name', $event->event_name, ['class' => 'form-control', 'placeholder'=>'Evento']) !!}
            {!! $errors->first('event_name', '<p class="alert alert-danger">:message</p>')!!}
          </div>
      </div>
    </div>

    <div class="col-12">
      <div class="form-group">
        <label for="start_date"> Data Início</label>
        <!-- {!!Form::label('start_date', 'Data de início:')!!} -->
        <!-- 2019/08/25 12:20 -->
        <div>
          <!-- <input type="datetime-local" class="form-control"  name="start_date"> -->
          {!!Form::input('dateTime-local','start_date', date('Y-m-d\TH:i', strtotime($event->start_date)), ['class' => 'form-control']) !!}
          {!! $errors->first('start_date', '<p class="alert alert-danger">:message</p>')!!}
        </div>
      </div>
    </div>

    <div class="col-12">
      <div class="form-group">
        <label for="end_date"> Data Final:</label>
        <!-- {!!Form::label('end_date', 'data do Término:')!!} -->
        <div>
          {!!Form::input('dateTime-local','end_date', date('Y-m-d\TH:i', strtotime($event->end_date)), ['class' => 'form-control']) !!}
          <!-- Form::time('time', \Carbon\Carbon::now()->timezone('Europe/Brussels')->format('H:i:s'),['class' => 'form-control']) -->
          {!! $errors->first('end_date', '<p class="alert alert-danger">:message</p>')!!}
        </div>
      </div>
    </div>
    <div class="col-12">
      <button type="submit" class="btn btn-primary add-event" value="" id="add-event">Alterar Evento</button>
    </div>
  
  {!! Form::close() !!}



@stop