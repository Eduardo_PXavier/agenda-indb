@extends('mobile.app')


@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-12 ">
      {!! csrf_field() !!}
      @include('flash')
      <h1 class="rounded-top" style="background: #504f4f54;padding: 
                    2px;margin-bottom: 0px;margin-top: 10px;color: #000;">
        Contatos: #{{$categoria->category}}
      </h1>



      @foreach($contacts as $contact)

      <div class="btnDelete container col-12" id="{{$contact->id}}" data-id="{{$contact->id}}" style="margin-bottom: 1vh;background-color: #81efb8e6;padding-bottom: 1vh;">
        <div class="textCenter">
          <a class="nav-link contato-link link-contato" href="{{route('contato.edit',$contact->id)}}">
            <p>{{ $contact->name }}
              <span style="font-size: 14px"> {{ $contact->last_name }}</span>
            </p>
          </a>
        </div>
        <div class="textCenter" style="font-size: 22px;">
          @if(is_null($contact->tels->first()))
          ----
          @else
          {{ $contact->tels->first()->tel}}
          @endif
        </div>

        <div class="textCenter">
          <!-- <button class="btn btn-danger delete-contact" value="{{$contact->id}}">Deletar</button>  -->
          <button type="button" class="btn btn-danger-tabela btnDelete" data-toggle="modal" data-id="" data-target="#myModal">Delete</button>
        </div>

      </div>

      @endforeach
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="divue">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Deletar?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="divue">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              tem certeza que deseja enviar contato para lixeira?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <!-- <button class="btn btn-danger delete-contact" value="">Deletar</button>  -->
              <button type="button" class="btn btn-danger delete-contact" value="" id="delete-btn">Deletar</button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>



<script>
  $('button.btnDelete').on('click', function() {
    
    console.log('teste');
    var idContato = $(this).closest('div.container').data('id');
    console.log(idContato);

    $('.delete-contact').click(function() {
      var id = idContato;
      console.log(id);
      console.log('teste');
      $.ajax({
        type: "POST",
        url: '/contato/' + id,
        data: {
          _method: 'delete',
          _token: "{{ csrf_token() }}"
        },
        success: function(data) {
          console.log(data);
          $("#" + id).remove();
          $('#myModal').modal('hide');

        },
        error: function(data) {
          console.log('Error:', data);
        }

      });

    });
  });
</script>
@stop