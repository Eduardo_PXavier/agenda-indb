@extends('mobile.app')


@section('content')
<div class="container-fluid">
  {!! csrf_field() !!}
  @include('flash')
  <div class="row">
    <div class="col-12">
      <h1 class="rounded-top col-12" style="background: #054a454d;padding: 
                    2px;margin-bottom: 0px;margin-top: 10px; color:#91fff2; ">
        Eventos:
        <button type="button" class="btn btn-warning voltar" value="" id="voltar">Voltar</button>
      </h1>


      @foreach($events as $event)
      
        <div class="btnDelete container col-12" id="{{$event->id}}" data-id="{{$event->id}}" style="margin-bottom: 1vh;background-color: #efbf81e6;padding-bottom: 1vh;">
          <div class="textCenter">
            <a class="nav-link contato-link link-contato" href="{{$event->id}}/edit">
              <p>
                {{ $event->event_name }}
              </p>
            </a>
          </div>
          <div class="textCenter" style="font-size: 22px;">
            {{ $event->start_date }}
          </div>
          <div class="textCenter" style="font-size: 22px;">
            {{ $event->end_date }}
          </div>
          <div class="textCenter">
            <!-- <button class="btn btn-danger delete-event" value="{{$event->id}}">Deletar</button>  -->
            <button type="button" class="btn btn-danger-tabela btnDelete" data-toggle="modal" data-id="" data-target="#myModal">Delete</button>
          </div>

        </div>
        
      @endforeach
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="divue">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Deletar?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="divue">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Tem certeza que deseja deletar evento?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <!-- <button class="btn btn-danger delete-event" value="">Deletar</button>  -->
              <button type="button" class="btn btn-danger delete-event" value="" id="delete-btn">Deletar</button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<script>
  $('button.btnDelete').on('click', function() {
    
    var idContato = $(this).closest('div.container').data('id');

    $('.delete-event').click(function() {
      var id = idContato;
      $.ajax({
        type: "POST",
        url: '/events/' + id,
        data: {
          _method: 'delete',
          _token: "{{ csrf_token() }}"
        },
        success: function(data) {
          $("#" + id).remove();
          $('#myModal').modal('hide');

        },

      });

    });
  });
</script>

<script>
  $('button.voltar').on('click', function() {
    
    window.location.href = 'http://m.agendaeduardo.com/events';
  });
</script>
@stop