
$(document).ready(function () {
    $('.masktelcontatos').mask('(00) 00 0000-0000');
});


$('button.btnDelete').on('click', function (e) {
    e.preventDefault();
    console.log('teste');
    var idContato = $(this).closest('tr').data('id');
    console.log(idContato);

    $('.delete-contact').click(function () {
        var id = idContato;
        console.log(id);
        console.log('teste');
        $.ajax({
            type: "POST",
            url: '/contato/' + id,
            data: {
                _method: 'delete',
                _token:  $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
                $("#" + id).remove();
                $('#myModal').modal('hide');

            },
            error: function (data) {
                console.log('Error:', data);
            }

        });

    });
});
