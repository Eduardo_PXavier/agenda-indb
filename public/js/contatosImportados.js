$('#selectAllImportados').click(function () {

    if ($(this).prop('checked')) {
        $('.check').prop('checked', true);
        $('.yourName').removeAttr("disabled");
        $('.yourLastName').removeAttr("disabled");
        $('.yourTel').removeAttr("disabled");
        $('.yourEmail').removeAttr("disabled");
    } else {
        $('.check').prop('checked', false);
        $('.yourName').attr("disabled", true);
        $('.yourLastName').attr("disabled", true);
        $('.yourTel').attr("disabled", true);
        $('.yourEmail').attr("disabled", true);
    }
});
