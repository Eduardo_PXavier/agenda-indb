<?php

namespace App;

use App\Contato;
use Illuminate\Database\Eloquent\Model;

class Cpf extends Model
{

    protected $fillable = [
        'cpf','contato_id'
    ];

    

    public function contato()
    {
        return $this->belongsTo(Contato::class);
    }

}