<?php

namespace App;

use App\Tel;
use App\User;
use App\Email;
use App\Address;
use App\Category;
use Illuminate\Database\Eloquent\Model;
use App\Notifications\NewContactCreated;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contato extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name','last_name','user_id'
    ];

    protected static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            $model->notify(new NewContactCreated());
        });
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function tels()
    {
        return $this->hasMany(Tel::class);
    }
    public function emails()
    {
        return $this->hasMany(Email::class);
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_contact', 'contact_id', 'category_id');
    }
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }
    public function cpfs()
    {
        return $this->hasMany(Cpf::class);
    }
    // link para slack
    public function routeNotificationForSlack($notification)
    {
        return 'https://hooks.slack.com/services/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';
    }
}
