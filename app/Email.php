<?php

namespace App;

use App\Contato;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'email','contato_id'
    ];

    

    public function contato()
    {
        return $this->belongsTo(Contato::class);
    }

}