<?php

namespace App\Http\Controllers;

use App\User;
use App\Contato;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ContatoRequest;

class CategoryController extends Controller
{
    public function postCategory(array $categories, int $idContato)
    {

        foreach ($categories as $category) {
            $user = Auth::user();
            if (!is_null($category)) {
                $categoryDoBanco = $user->categories->contains('category', $category);
                if (!$categoryDoBanco) {
                    $dadosCategory = [
                        'user_id' => Auth::user()->id,
                        'category' => $category,

                    ];
                    $newCategory = Category::create($dadosCategory);
                    $newCategory->contato()->attach($idContato);
                } else {
                    $categoryToAttach = $user->categories->where('category', $category)->first();
                    $categoryToAttach->contato()->attach($idContato);
                }
            }
        }
    }

    public function deleteCategory($idCategory, $idContato)
    {
        $category = Category::findOrFail($idCategory);
        $category->contato()->detach($idContato);
    }

    public function updateCategory(array $categories, int $idContato)
    {
        $user = Auth::user();
        $contatos = Contato::find($idContato);
        // dd($categories);
        // $contatos->categories()->detach();
        $contatos->categories()->detach();
        foreach ($categories as $id => $category) {
            // verifica se o contato tem a categoria
            $contatoTemCategoria = $contatos->categories->contains('category', $category);
            // se nao tiver a categoria
            if (!$contatoTemCategoria) {
                // verifica se o input é null
                if (!is_null($category)) {
                    // verifica se categoria do input ja existe
                    $categoryDoBanco = $user->categories->contains('category', $category);
                    // dd($categoryDoBanco);
                    // se existe
                    if ($categoryDoBanco) {
                        // da o attach na categoria existente
                        $categoryToAttach = $user->categories->where('category', $category)->first();
                        $categoryToAttach->contato()->attach($idContato);
                    }
                    // se não existe
                    if (!$categoryDoBanco) {
                        // cria a categoria e da o attach
                        $dadosCategory = [
                            'user_id' => Auth::user()->id,
                            'category' => $category,
                        ];
                        $newCategory = Category::create($dadosCategory);
                        $newCategory->contato()->attach($idContato);
                    }
                }
            }
        }
        $categoriasUser = $user->categories->all();
        // dd($categoriasUser);
        foreach ($categoriasUser as $categoria) {
            $verifica = $categoria->contato->first();
            if (is_null($verifica)) {
                $categoria->delete();
            }
        }
    }
    public function contatosCategoria($nameCategory)
    {
        $user = Auth::user();
        $categoria = $user->categories->where('category', $nameCategory)->first();
        if ($categoria) {
            $contacts = $categoria->contato;
            // dd($contacts);
            $routeName = URL::current();
            
            if ($routeName == "http://agendaeduardo.com/contatos/".$nameCategory) {
                return view('paginas.pesquisaCategoria', compact('contacts', 'categoria'));
            }
            if ($routeName == "http://m.agendaeduardo.com/contatos/".$nameCategory) {
                return view('mobile.pesquisaCategoria', compact('contacts', 'categoria'));
            }
            
        } else {
            return back();
        }
    }

    public function index()
    {
        $user = User::find(Auth::user()->id);
        $categorias = $user->categories->all();
        return view('paginas.categoria', compact('categorias'));
    }
}
