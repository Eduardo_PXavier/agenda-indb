<?php

namespace App\Http\Controllers;

use App\User;
use App\Mailers\AppMailer;
use Illuminate\Support\Str;
use App\Http\Requests\RegisterRequest;

class RegistrationController extends Controller
{
    public function cadastrar()
    {
        return view('auth.register');
    }
    public function postRegister(RegisterRequest $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => $request['password'],
            'api_token' => Str::random(60)
            ]);
        return redirect('login');
    }
}
