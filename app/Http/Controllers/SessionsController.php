<?php namespace app\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class SessionsController extends Controller
{

    public function login()
    {
         return view('auth.login');
    }

    public function postLogin(Request $request)
    {

        $this->validate($request, ['email' => 'required|email', 'password' => 'required']);
        
        if (Auth::attempt($this->getCredentials($request))) {
            flash('Você está logado');
            return redirect()->route('contatos');
        }
        flashdanger('Login negado: verifique se email e senha estão corretos');

        return redirect('entrar');
    }

    public function logout()
    {
        Auth::logout();

        flash('Até a proxima');
        return redirect('login');
    }

    protected function getCredentials(Request $request)
    {
        return[
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];
    }
}