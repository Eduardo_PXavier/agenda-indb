<?php

namespace App\Http\Controllers;

use Auth;
use Calendar;
use App\Event;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;

class EventController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $events = $user->events->all();

        $event_list = [];
        foreach ($events as $key => $event) {
            $event_list[] = Calendar::event(
                $event->event_name,
                false,
                new \DateTime($event->start_date),
                new \DateTime($event->end_date)
            );
        }
        $calendar_details = Calendar::addEvents($event_list);
        $routeName = URL::current();

        if ($routeName == "http://agendaeduardo.com/events") {
            return view('events', compact('calendar_details'));
        }
        if ($routeName == "http://m.agendaeduardo.com/events") {
            return view('mobile.events', compact('calendar_details'));
        }
    }
    public function store(Request $request)
    {
        
        $user = Auth::user()->id;
        $validator = Validator::make($request->all(), [
            'event_name' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ]);
        if ($validator->fails()) {
            if (request()->ajax()) {
                // echo json_encode(array('status' => 'error','message'=> 'Inputs estão incompletos'));
                return response()->json(['menssage' => 'Verifique se todos os valores foram colocados corretamente']);
            }
            // \Session::flash('warning', 'Please enter the valid details');
            // return Redirect::to('/events')->withInput()->withErrors($validator);
        }

        $event = new Event;

        $event->event_name = $request['event_name'];
        $event->start_date = $request['start_date'];
        $event->end_date = $request['end_date'];
        $event->user_id = $user;
        // dd($event->user_id);
        $event->save();

        if (request()->ajax()) {
            return response()->json(['menssage' => 'Evento adicionado com sucesso']);
        }

        return Redirect::to('/events');
    }

    public function show(Request $request)
    {
        $user = Auth::user();
        $events = $user->events->all();
        $routeName = URL::current();
        if ($routeName == "http://agendaeduardo.com/events/list") {
            return view('paginas.listEvent', compact('events'));
        }
        if ($routeName == "http://m.agendaeduardo.com/events/list") {
            return view('mobile.listEvent', compact('events'));
        }
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $event = $user->events->find($id);
        $event->delete();
        
        return redirect('events/list');
    }

    public function edit($id)
    {
        $user = Auth::user();
        $event = $user->events->find($id);
        // dd($event->start_date);
        $routeName = URL::current();
        if ($routeName == "http://agendaeduardo.com/events/".$id."/edit") {
            return view('paginas.editEvent', compact('event'));
        }
        if ($routeName == "http://m.agendaeduardo.com/events/".$id."/edit") {
            return view('mobile.editEvent', compact('event'));
        }
        
    }

    public function update($id, Request $request)
    {
        $evento = Event::find($id);
        $evento->event_name = $request->event_name;
        $evento->start_date = $request->start_date;
        $evento->end_date = $request->end_date;
        $evento->save();
        return redirect('events');
    }
}
