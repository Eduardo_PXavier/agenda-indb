<?php

namespace App\Http\Controllers;

use App\Email;
use App\Contato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ContatoRequest;

class EmailController extends Controller
{
    public function postEmail(array $emails, int $id)
    {
        $user=Auth::user();
        $contatos=$user['contatos'];
        
        foreach ($emails as $email) {
            if (!is_null($email)) {
                if ($email) {
                    $dadosEmail = [

                        'contato_id' => $id,
                        'email' => $email,

                    ];
                    Email::create($dadosEmail);
                }
            }
        }
    }
    public function deleteEmail($id)
    {
        $email = Email::findOrFail($id);
        $email->delete();
    }

    public function updateEmail(array $emails, int $idContato)
    {


        foreach ($emails as $id => $email) {
            // dd($emails);
            if (!is_null($email)) {
                // dd($email);
                $emailDoBanco = Email::find($id);
                // dd($emailDoBanco);
                if (!is_null($emailDoBanco)) {
                    $emailDoBanco->email = $email;
                    $emailDoBanco->save();
                }
                if (is_null($emailDoBanco)) {
                    $dadosEmail = [

                        'contato_id' => $idContato,
                        'email' => $email,

                    ];

                    Email::create($dadosEmail);
                }
            } else {
                $emailsDoBanco = Email::where('contato_id', $idContato)->get();
                // dd($emailsDoBanco);
                $emailsDoBanco->each->delete();
            }
        }
    }
}
