<?php

namespace App\Http\Controllers;

use App\Tel;
use App\User;
use App\Email;
use \Validator;
use App\Address;
use App\Contato;
use Artdarek\OAuth\OAuth;
use Illuminate\Http\Request;
use App\Exports\ContactsExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\TelController;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\URL;

class ContatoController extends Controller
{

    public function index()
    {
        $routeName = URL::current();
        if ($routeName=="http://agendaeduardo.com/contato") {
            return view('paginas.novo-contato');
        }
        if ($routeName=="http://m.agendaeduardo.com/contato") {
            return view('mobile.novo-contato');
        }
    }

    public function store(ContatoRequest $request)
    {
        $user = Auth::user();
        $validationEmail = false;
        $validationCpf = false;
        $input['email'] = Input::get('email');
        $input['cpf'] = Input::get('cpf');
        $conts = Auth::user()->contatos;
        $ContatoIgualEmail = "";
        $ContatoIgualCpf = "";
        foreach ($input['email'] as $em) {
            foreach ($conts as $cont) {
                $emails = $cont->emails->pluck('email');

                if ($emails->contains($em)) {
                    $validationEmail = true;

                    $ContatoIgualEmail .= $cont->name . ' ' . $cont->last_name . '; ';
                }
            }
        }
        foreach ($conts as $cont) {
            $cpfs = $cont->cpfs->pluck('cpf');
            // dump($cont->cpfs->pluck('cpf'));

            if ($cpfs->contains(preg_replace('/[^0-9]/', '', $input['cpf'][0]))) {
                $validationCpf = true;
                $ContatoIgualCpf .= $cont->name . ' ' . $cont->last_name . '; ';
            }
        }

        // dd($cpfs->contains(preg_replace('/[^0-9]/', '', $input['cpf'][0])));
        if ($validationEmail) {
            flashdanger('Email já existente para o contato: ' . $ContatoIgualEmail);
            return Redirect::back();
        }
        if ($validationCpf) {
            flashdanger('Cpf já existente para o contato: ' . $ContatoIgualCpf);
            return Redirect::back();
        } elseif (!$validationCpf && !$validationEmail) {
            $dadosContatos = [
                'user_id' => Auth::user()->id,
                'name' => $request->input('name'),
                'last_name' => $request->input('last_name')
            ];

            $contato = Contato::create($dadosContatos);

            app(TelController::class)->postTel($request->input('tel'), $contato->id);
            app(EmailController::class)->postEmail($request->input('email'), $contato->id);
            app(CategoryController::class)->postCategory($request->input('category'), $contato->id);
            app(CpfController::class)->postCpf($request->input('cpf'), $contato->id);
            app(AddressController::class)
                ->postAddress(
                    $request->input('city'),
                    $request->input('state'),
                    $request->input('cep'),
                    $request->input('street'),
                    $request->input('neighborhood'),
                    $request->input('number'),
                    $contato->id
                );

            return redirect('contatos');
        }
    }

    public function show($id)
    {
        // dd('teste');
        // $user = User::find(Auth::user()->id);
        // $contacts = $user->contatos->all();
        // return view('paginas.contatos', compact('contacts'));
    }
    public function edit($id)
    {
        $user = Auth::user();
        $contact = $user->contatos->find($id);
        // dd($contact);
        // $contact = Contato::findOrFail($id);
        $routeName = URL::current();
        if ($routeName=="http://agendaeduardo.com/contato/".$id."/edit") {
            return view('paginas.edit-contato', compact('contact'));
        }
        if ($routeName=="http://m.agendaeduardo.com/contato/".$id."/edit") {
            return view('mobile.edit-contato', compact('contact'));
        }
    }

    public function update($id, ContatoRequest $request)
    {
        $user = Auth::user();
        $validationEmail = false;
        $validationNewEmail = false;
        $validationCpf = false;
        $input['email'] = Input::get('email');
        $input['newEmail'] = Input::get('newEmail');
        $input['cpf'] = Input::get('cpf');
        $conts = Auth::user()->contatos;
        $ContatoIgualEmail = "";
        $ContatoIgualCpf = "";
        $aux = 0;
        $aux2 = 0;
        $tamanho = count($input['email']);
        
        foreach ($input['email'] as $em) {
            foreach ($conts as $cont) {
                $emails = $cont->emails->pluck('email');
                if ($emails->contains($em)) {
                    $aux++;
                    $ContatoIgualEmail .= $cont->name . ' ' . $cont->last_name . '; ';
                    // dump($tamanho);
                    // dump($em);
                    if ($aux > $tamanho) {
                        $validationEmail = true;
                        dump( $validationEmail);
                    }
                }
            }
          
            if (count(array_keys($input['email'], $em)) >=2 ){
                $ContatoIgualEmail ="Email ja existe para esse contato";
                $validationEmail = true;
            }
            
            // dump($em);
        }
        // die;
        foreach ($input['newEmail'] as $em) {
            foreach ($conts as $cont) {
                $emails = $cont->emails->pluck('email');
                
                if ($emails->contains($em)) {
                    $aux++;
                    
                    $ContatoIgualEmail .= $cont->name . ' ' . $cont->last_name . '; ';
                    if ($aux >= $tamanho) {
                        $validationNewEmail = true;
                    }
                }
            }
            if (count(array_keys($input['newEmail'], $em)) >= 2){
                $ContatoIgualEmail ="Emails iguais";
                $validationNewEmail = true;
            }
        }
        // die;
        
        // die;
        foreach ($input['cpf'] as $fcpf) {
            // dump($input['cpf']);
            foreach ($conts as $cont) {
                $cpfs = $cont->cpfs->pluck('cpf');
                if ($cpfs->contains(preg_replace('/[^0-9]/', '', $fcpf))) {
                    $aux2++;
                    
                    $ContatoIgualCpf .= $cont->name . ' ' . $cont->last_name . '; ';
                    if ($aux2 > 1) {
                        $validationCpf = true;
                    }
                    if($aux2 = 1) {
                        if ($cont->cpfs->where('cpf', $fcpf)[0]->contato_id != $id) {
                            $validationCpf = true;
                        }
                    }
                }
            }
        }
        // die;
        if($validationNewEmail) {
            flashdanger('Email já existente para os contatos: ' . $ContatoIgualEmail);
            return Redirect::back();
        }
        if ($validationEmail) {
            flashdanger('Email já existente para os contatos: ' . $ContatoIgualEmail);
            return Redirect::back();
        }
        if ($validationCpf) {
            flashdanger('Cpf já existente para o contato: ' . $ContatoIgualCpf);
            return Redirect::back();
        } elseif (!$validationCpf && !$validationEmail) {
            $contact = Contato::findOrFail($id);

            Tel::onlyTrashed()
                ->where('contato_id', $id)
                ->forceDelete();
            Email::onlyTrashed()
                ->where('contato_id', $id)
                ->forceDelete();
            Address::onlyTrashed()
                ->where('contato_id', $id)
                ->forceDelete();

            $this->validate($request, [
                'name' => 'required',
                'last_name' => 'required',
            ]);
            // dd($request->all());
            // dd($contact->addresses->all());

            app(TelController::class)->updateTel($request->input('tel'), $contact->id);
            app(TelController::class)->postTel($request->input('newTel'), $contact->id);
            app(EmailController::class)->updateEmail($request->input('email'), $contact->id);
            app(EmailController::class)->postEmail($request->input('newEmail'), $contact->id);
            app(CategoryController::class)->updateCategory($request->input('category'), $contact->id);
            app(CategoryController::class)->postCategory($request->input('newCategory'), $contact->id);
            app(CpfController::class)->updateCpf($request->input('cpf'), $contact->id);
            if (
                !is_null($request->input('city'))
                && !is_null($request->input('state'))
                && !is_null($request->input('street'))
                && !is_null($request->input('neighborhood'))
                && !is_null($request->input('cep'))
                && !is_null($request->input('number'))
            ) {
                app(AddressController::class)
                    ->updateAddress(
                        $request->input('city'),
                        $request->input('state'),
                        $request->input('cep'),
                        $request->input('street'),
                        $request->input('neighborhood'),
                        $request->input('number'),
                        $contact->id
                    );
            }

            app(AddressController::class)
                ->postAddress(
                    $request->input('newCity'),
                    $request->input('newState'),
                    $request->input('newCep'),
                    $request->input('newStreet'),
                    $request->input('newNeighborhood'),
                    $request->input('newNumber'),
                    $contact->id
                );
            $contact->fill($request->all())->save();
            // dd($contact);
            return redirect('contatos');
        }
    }

    public function destroy($id)
    {
        $contatos = Contato::findOrFail($id);
        $categorias = $contatos->categories->all();
        
        $contatos->delete();
        foreach ($categorias as $categoria) {
            $verifica = $categoria->contato->first();
            if (is_null($verifica)) {
                $categoria->delete();
            }
        }

        return redirect('contatos');
    }
    public function forceDestroy($id)
    {
        Contato::withTrashed()
            ->where('id', $id)
            ->forceDelete();

        $user = User::find(Auth::user()->id);
        $categorias = $user->categories->all();
        foreach ($categorias as $categoria) {
            $verifica = $categoria->contato->first();
            if (is_null($verifica)) {
                $categoria->delete();
            }
        }

        return redirect('lixeira');
    }

    public function trash()
    {
        $contacts = Contato::onlyTrashed()
            ->where('user_id', Auth::user()->id)
            ->get();
        // dd($contacts);
        $routeName = URL::current();
        if ($routeName=="http://agendaeduardo.com/lixeira") {
            return view('paginas.lixeira', compact('contacts'));
        }
        if ($routeName=="http://m.agendaeduardo.com/lixeira") {
            return view('mobile.lixeira', compact('contacts'));
        }
        
    }
    public function restore($id)
    {
        $contacts = Contato::withTrashed()
            ->where('id', $id)
            ->restore();
        // dd($contacts);
        return redirect('lixeira');
    }

    public function restoreAll(Request $request)
    {
        // return ($request);
        Tel::withTrashed()
            ->where('contato_id', $request->input('id'))
            ->restore();
        Email::withTrashed()
            ->where('contato_id', $request->input('id'))
            ->restore();
        Address::withTrashed()
            ->where('contato_id', $request->input('id'))
            ->restore();
    }

   

    public function importGoogleContact(Request $request)
    {
        // get data from request
        $code = $request->get('code');

        // get google service
        $googleService = \OAuth::consumer('Google');

        // check if code is valid

        // if code is provided get user data and sign in
        if (!is_null($code)) {

            // This was a callback request from google, get the token
            try {
                $token = $googleService->requestAccessToken($code);
            } catch (\Exception $e) {
                return redirect("https://accounts.google.com/signin/oauth/oauthchooseaccount?client_id=989697552466-6pqteu355thbttkn5vrnsjq8qt0p75p2.apps.googleusercontent.com&as=3quEukUejjYYpTS3KABuvA&destination=http%3A%2F%2F127.0.0.1%3A8000&approval_state=!ChR2SlhrMXRrUGo1VFlsUnFCZ3Q5RxIfTS1WNTNtaDdVUzhZRUZvSEdUZHlJLTJkWEQzVXp4WQ%E2%88%99AJDr988AAAAAXXFCGUCySm56Q8YINbAjl4RRx0ywyd25&oauthgdpr=1&oauthriskyscope=1&xsrfsig=ChkAeAh8TxHun7fQPXBDV0mMTK5bezC4D60PEg5hcHByb3ZhbF9zdGF0ZRILZGVzdGluYXRpb24SBXNvYWN1Eg9vYXV0aHJpc2t5c2NvcGU&flowName=GeneralOAuthFlow");
            }
            // dd($token);
            // Send a request with it
            $result = json_decode($googleService->request('https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=500'), true);
            // dd($result);
            // Going through the array to clear it and create a new clean array with only the email addresses

            // initialize the new array
            $names = [];
            $lastnames = [];
            $emails = [];
            $tels = [];

            foreach ($result['feed']['entry'] as $contact) {
                // dd($contact);
                if ($contact['title']['$t'] != "") {
                    $splitName =  explode(' ', $contact['title']['$t'], 2);
                    // dump( $splitName );
                    $names[] = $splitName[0];
                    $lastnames[] = !empty($splitName[1]) ? $splitName[1] : '';
                } else {
                    $names[] = null;
                    $lastnames[] = null;
                }
                if (isset($contact['gd$email'])) { // Sometimes, a contact doesn't have email address
                    $emails[] = $contact['gd$email'][0]['address'];
                } else {
                    $emails[] = null;
                }
                if (isset($contact['gd$phoneNumber'])) {
                    $tels[] = preg_replace('/[^0-9]/', '', $contact['gd$phoneNumber'][0]['$t']);
                } else {
                    $tels[] = null;
                }
            }

            // dd($names);
            $contatos = [];

            for ($i = 0; $i < count($names); ++$i) {
                $contatos[] = [
                    'name' => $names[$i],
                    'lastname' => $lastnames[$i],
                    'email' => $emails[$i],
                    'tel' => $tels[$i]
                ];
            }

            // dd($contatos);
            return view('paginas.contatosImportados', compact('contatos'));
        } else { // if not ask for permission first
            
            $url = $googleService->getAuthorizationUri();
           
            return redirect((string) $url);
        }
    }

    public function importadosGoogle(Request $request)
    {

        $Nomes = $request->Nome;
        $SobreNomes = $request->SobreNome;
        $Tels = $request->Tel;
        $Emails = $request->Email;


        for ($i = 0; $i < count($Nomes); ++$i) {
            $NovoContato = [];
            $NovoTel = [];
            $NovoEmail = [];
            if (!is_null($Nomes[$i])) {
                $NovoContato = [
                    'user_id' => Auth::user()->id,
                    'name' => $Nomes[$i],
                ];
            } else {
                $NovoContato += [
                    'user_id' => Auth::user()->id,
                    'name' => 'xxxx',
                ];
            }

            if (!is_null($SobreNomes[$i])) {
                $NovoContato += [
                    'last_name' => $SobreNomes[$i],
                ];
            } else {
                $NovoContato += [
                    'last_name' => 'xxxx',
                ];
            }
            $contato = Contato::create($NovoContato);
            // dd($contato->id);
            if (!is_null($Tels[$i])) {
                $NovoTel = [
                    'contato_id' => $contato->id,
                    'tel' => $Tels[$i],
                ];
            } else {
                $NovoTel = [
                    'contato_id' => $contato->id,
                    'tel' => "xxxx",
                ];
            }
            $tel = Tel::create($NovoTel);
            if (!is_null($Emails[$i])) {
                $NovoEmail = [
                    'contato_id' => $contato->id,
                    'email' => $Emails[$i],
                ];
            } else {
                $NovoEmail = [
                    'contato_id' => $contato->id,
                    'email' => 'xxxx',
                ];
            }
            $email = Email::create($NovoEmail);
        }
        $routeName = URL::current();
        $m='';
        if ($routeName=="http://agendaeduardo.com/contato/google") {
            $m='';
        }
        if ($routeName=="http://m.agendaeduardo.com/contatos/google") {
            $m='m.';
        }
        
        return redirect('https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://'.$m.'agendaeduardo.com/contatos');
    }

    public function excel()
    {
        $contatos = Auth::user()->contatos;
        return view('paginas.excel', compact('contatos'));
    }

    public function export()
    {
        return Excel::download(new ContactsExport, 'contatos.xlsx');
    }

   
}
