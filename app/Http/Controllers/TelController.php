<?php

namespace App\Http\Controllers;

use App\Tel;
use App\Contato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ContatoRequest;

class TelController extends Controller
{
    public function postTel(array $telefones, int $id)
    {
        
        foreach ($telefones as $telefone) {
            if (!is_null($telefone)) {
                $dadosTel = [

                    'contato_id' => $id,
                    'tel' => $telefone,
                    
                ];
                
                Tel::create($dadosTel);
            }
        }
    }
    public function deleteTel($id)
    {
        $tel= Tel::findOrFail($id);
        $tel->delete();
    }

    public function updateTel(array $telefones, int $idContato)
    {
        foreach ($telefones as $id => $telefone) {
            
            if (!is_null($telefone)) {
                $tel = Tel::find($id);
                if (!is_null($tel)) {
                    $tel->tel=$telefone;
                    $tel->save();
                }
                if (is_null($tel)) {
                    $dadosTel = [

                        'contato_id' => $idContato,
                        'tel' => $telefone,
                        
                    ];
                    
                    Tel::create($dadosTel);
                }
            }
        }
    }
}

