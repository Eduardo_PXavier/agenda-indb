<?php

namespace App\Http\Controllers;

use App\User;
use App\Contato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
class CadastradoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {

    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function contatos()
    {
        $routeName = URL::current();
        
        $user = Auth::user();
        $contacts = Contato::all()->sortBy('name')->where("user_id", $user->id)->all();
        if ($routeName=="http://agendaeduardo.com/contatos") {
            return view('paginas.contatos', compact('contacts'));
        }
        if ($routeName=="http://m.agendaeduardo.com/contatos") {
            return view('mobile.contatos', compact('contacts'));
        }
    }
    // public function contatosMobile()
    // {
    //     $user = Auth::user();
    //     $contacts = Contato::all()->sortBy('name')->where("user_id", $user->id)->all();
    //     return view('paginas.contatos', compact('contacts'));
    // }
}
