<?php

namespace App\Http\Controllers;

use App\Cpf;
use App\Email;
use App\Contato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ContatoRequest;

class CpfController extends Controller
{
    public function postCpf(array $cpfs, int $id)
    {
        foreach ($cpfs as $cpf) {
            if (!is_null($cpf)) {
                $dadosCpf = [

                    'contato_id' => $id,
                    'cpf' => $cpf,
                    
                ];
                
                Cpf::create($dadosCpf);
            }
        }
    }

    public function updateCpf(array $cpfs, int $idContato)
    {
        
       
        foreach ($cpfs as $id => $cpf) {
            // dd($id);
            if (!is_null($cpf)) {
                // dd($cpf);
                $cpfDoBanco = Cpf::find($id);
                // dd($cpfDoBanco);
                if(!is_null($cpfDoBanco)){
                    $cpfDoBanco->cpf= $cpf;
                    $cpfDoBanco->save();
                }
                if(is_null($cpfDoBanco)){
                    $dadosCpf = [

                        'contato_id' => $idContato,
                        'cpf' => $cpf,
                        
                    ];
                    
                    Cpf::create($dadosCpf);
                }
            }
            else {
                $cpfsDoBanco = Cpf::where('contato_id', $idContato)->get();
                // dd($cpfsDoBanco);
                $cpfsDoBanco->each->delete();
            }
        }
        

    }
}

