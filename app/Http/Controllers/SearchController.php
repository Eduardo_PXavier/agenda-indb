<?php

namespace App\Http\Controllers;

use App\Email;
use App\Address;
use App\Contato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function search(Request $request)
    {

        $search = $request->get('search');
        
        $user = Auth::user();
        $resultadoFinal = [];
        $resultadoFinalEmail = [];
        $resultadoFinalTel = [];
        $resultadoFinalCpf = [];
        $resultadoFinalAddress = [];
        $resultadoFinalContatos = [];
        if (!is_null($search)) {
            $resultadoNome = Contato::where('name', 'like', '%' . $search . '%')
                ->orWhere('last_name', 'like', '%' . $search . '%')->get();
            $resultadoCategories = $user->categories()->where('category', 'like', '%' . $search . '%')->get();
            $resultadoEventos = $user->events()->where('event_name', 'like', '%' . $search . '%')
                ->orWhere('start_date', 'like', '%' . $search . '%')
                ->orWhere('end_date', 'like', '%' . $search . '%')
                ->get();
            $resultadoAddress = Address::Where('city', 'like', '%' . $search . '%')
                ->orWhere('state', 'like', '%' . $search . '%')
                ->orWhere('street', 'like', '%' . $search . '%')
                ->orWhere('neighborhood', 'like', '%' . $search . '%')
                ->orWhere('cep', 'like', '%' . $search . '%')
                ->get();
            $resultadoEmails = Email::where('email', 'like', '%' . $search . '%')->get();
            foreach ($user->contatos as $contato) {
                $resultadoTel = $contato->tels()->where('tel', 'like', '%' . $search . '%')->get();
                $resultadoCpf = $contato->cpfs()->where('cpf', 'like', '%' . $search . '%')->get();

                $resultadoFinalEmail = array_merge($resultadoFinalEmail, $resultadoEmails->where('contato_id', $contato->id)->all());
                if (!$resultadoTel->isEmpty()) {
                    $resultadoFinalTel = array_merge($resultadoFinalTel, $resultadoTel->all());
                }
                if (!$resultadoCpf->isEmpty()) {
                    $resultadoFinalCpf = array_merge($resultadoFinalCpf, $resultadoCpf->all());
                }
                $resultadoFinalAddress = array_merge($resultadoFinalAddress, $resultadoAddress->where('contato_id', $contato->id)->all());
            }
            $resultadoFinalContatos = array_merge($resultadoFinalContatos, $resultadoNome->where('user_id', $user->id)->all());
            // dump($search);
            // die;
            $routeName = URL::current();
            
            if ($routeName == "http://agendaeduardo.com/search") {
                return view('paginas.pesquisa', compact(
                    'search',
                    'resultadoFinalContatos',
                    'resultadoFinalAddress',
                    'resultadoFinalCpf',
                    'resultadoFinalTel',
                    'resultadoFinalEmail',
                    'resultadoCategories',
                    'resultadoEventos'
                ));
            }
            if ($routeName == "http://m.agendaeduardo.com/search") {
                return view('mobile.pesquisa', compact(
                    'search',
                    'resultadoFinalContatos',
                    'resultadoFinalAddress',
                    'resultadoFinalCpf',
                    'resultadoFinalTel',
                    'resultadoFinalEmail',
                    'resultadoCategories',
                    'resultadoEventos'
                ));
            }
            
        }
        flashDanger('Insira um valor no campo de busca');
        return redirect()->back();
    }
}
