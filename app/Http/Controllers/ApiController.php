<?php

namespace App\Http\Controllers;

use App\Cpf;
use App\Tel;
use App\User;
use App\Email;
use App\Address;
use App\Contato;
use App\Category;
use App\exceptions\Handler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Validator;

class ApiController extends Controller
{

    public function contatos(Request $request)
    {

        $header = $request->header('token');
        if (!is_null($header)) {
            $user = User::where('api_token', $header)->first();
            if ($user) {
                $contatos = $user->contatos;
                return response()->json($contatos);
            } else {
                abort(403, 'Unauthorized');
            }
        } else {
            abort(403, 'Unauthorized');
        }
    }


    public function edit($id, Request $request)
    {

        $header = $request->header('token');

        $Name = $request->json("name");
        $LastName = $request->json("last_name");
        $Tel = $request->json("tel");
        $telId = $request->json("tel_id");
        $Email = $request->json("email");
        $emailId = $request->json("email_id");
        $CPF = $request->json("cpf");
        $cpfId = $request->json("cpf_id");
        $Category = $request->json("category");
        $categoryId = $request->json("category_id");
        $Cep = $request->json("cep");
        $Cidade = $request->json("cidade");
        $UF = $request->json("uf");
        $Rua = $request->json("rua");
        $Bairro = $request->json("bairro");
        $Numero = $request->json("numero");
        $addressId = $request->json("address_id");





        // verifica header
        if (!$header == "") {
            $user = User::where('api_token', $header)->first();
            $contato = $user->contatos->find($id);
            $telefone = $contato->tels->find($telId);
            $email = $contato->emails->find($emailId);
            $cpf = $contato->cpfs->find($cpfId);
            $category = $contato->categories->find($categoryId);
            $address = $contato->addresses->find($addressId);
            // verifica user
            if ($user) {
                // verifica contato
                if (!is_null($contato)) {
                    // verfica inputs nome sobrenome e telefone
                    if (!is_null($Name) && !is_null($LastName) && !is_null($Tel)) {
                        $contato->name = $Name;
                        $contato->last_name = $LastName;
                        $contato->save();
                        if ($telefone) {
                            $telefone->tel = $Tel;
                            $telefone->save();
                        }
                        if (!is_null($Email)) {
                            if ($email) {
                                $email->email = $Email;
                                $email->save();
                            }
                        }
                        if (!is_null($Category)) {
                            if ($category) {
                                $category->category = $Category;
                                $category->save();
                            }
                        }
                        if (!is_null($CPF)) {
                            if ($cpf) {
                                $cpf->cpf = $CPF;
                                $cpf->save();
                            }
                        }
                        if (!is_null($Cep)) {
                            if ($address) {
                                $address->cep = $Cep;
                                $address->save();
                            }
                        }
                        if (!is_null($Cidade)) {
                            if ($address) {
                                $address->city = $Cidade;
                                $address->save();
                            }
                        }
                        if (!is_null($UF)) {
                            if ($address) {
                                $address->state = $UF;
                                $address->save();
                            }
                        }
                        if (!is_null($Rua)) {
                            if ($address) {
                                $address->street = $Rua;
                                $address->save();
                            }
                        }
                        if (!is_null($Bairro)) {
                            if ($address) {
                                $address->neighborhood = $Bairro;
                                $address->save();
                            }
                        }
                        if (!is_null($Numero)) {
                            if ($address) {
                                $address->number = $Numero;
                                $address->save();
                            }
                        }
                    }

                    return response()->json(['Contato editado:' => $contato]);
                } else {
                    abort(403, 'Unauthorized');
                }
            } else {
                abort(403, 'Unauthorized');
            }
        } else {
            abort(403, 'Unauthorized');
        }
    }

    public function novo(Request $request)
    {
        $header = $request->header('token');
        $Name = $request->json("name");
        $LastName = $request->json("last_name");
        $Tel = $request->json("tel");
        $Email = $request->json("email");
        $CPF = $request->json("cpf");
        $Category = $request->json("category");
        $Cep = $request->json("cep");
        $Cidade = $request->json("cidade");
        $UF = $request->json("uf");
        $Rua = $request->json("rua");
        $Bairro = $request->json("bairro");
        $Numero = $request->json("numero");
        // dd($request->json("tel"));
        if (!is_null($header)) {
            $user = User::where('api_token', $header)->first();
            // dd($user->id);
            if ($user) {
                if (!is_null($Name) && !is_null($LastName) && !is_null($Tel)) {
                    // cria contato
                    $dadosContatos = [
                        'user_id' => $user->id,
                        'name' => $Name,
                        'last_name' => $LastName
                    ];
                    $contato = Contato::create($dadosContatos);
                    // cria telefone
                    $dadosTel = [

                        'contato_id' => $contato->id,
                        'tel' => $Tel,

                    ];
                    Tel::create($dadosTel);

                    // cria email
                    if (!is_null($Email)) {
                        $dadosEmail = [
                            'contato_id' => $contato->id,
                            'email' => $Email
                        ];
                        Email::create($dadosEmail);
                    }
                    // cria categoria
                    if (!is_null($Category)) {
                        $dadosCategory = [
                            'user_id' => $user->id,
                            'contato_id' => $contato->id,
                            'category' => $Category
                        ];
                        $newCategory = Category::create($dadosCategory);
                        $newCategory->contato()->attach($contato->id);
                    }
                    // cria cpf
                    if (!is_null($CPF)) {
                        $dadosCPF = [
                            'contato_id' => $contato->id,
                            'cpf' => $CPF
                        ];
                        Cpf::create($dadosCPF);
                    }
                    // cria endereço
                    if (!is_null($Cep) || !is_null($Cidade) || !is_null($UF) || !is_null($Rua) || !is_null($Bairro) || !is_null($Numero)) {
                        $dadosAddress = [
                            'contato_id' => $contato->id,
                            'city' => $Cidade,
                            'state' => $UF,
                            'street' => $Rua,
                            'neighborhood' => $Bairro,
                            'cep' => $Cep,
                            'number' => $Numero,
                        ];
                        Address::create($dadosAddress);
                    }


                    $contato->tels;
                    $contato->emails;
                    $contato->categories;
                    $contato->cpfs;
                    $contato->addresses;

                    return response()->json(['Novo Contato' => $contato]);
                } else {
                    abort(403, 'Nome, Sobre Nome e Telefone são obrigatorios');
                }
            }
        }
    }

    public function novosDados($id, Request $request)
    {
        $header = $request->header('token');

        $Tel = $request->json("tel");
        $Email = $request->json("email");
        $Category = $request->json("category");
        $Cep = $request->json("cep");
        $Cidade = $request->json("cidade");
        $UF = $request->json("uf");
        $Rua = $request->json("rua");
        $Bairro = $request->json("bairro");
        $Numero = $request->json("numero");
        $CPF = $request->json("cpf");

        if (!is_null($header)) {
            $user = User::where('api_token', $header)->first();



            if (!is_null($user)) {
                $contato = $user->contatos->find($id);
                if (!is_null($contato)) {
                    if (!is_null($Tel)) {
                        $dadosTel = [

                            'contato_id' => $contato->id,
                            'tel' => $Tel,

                        ];
                        Tel::create($dadosTel);
                    }
                    if (!is_null($Email)) {
                        $dadosEmail = [
                            'contato_id' => $contato->id,
                            'email' => $Email
                        ];
                        Email::create($dadosEmail);
                    }
                    if (is_null($contato->cpfs->first())) {
                        if (!is_null($CPF)) {
                            $dadosCPF = [
                                'contato_id' => $contato->id,
                                'cpf' => $CPF
                            ];
                            Cpf::create($dadosCPF);
                        }
                    }
                    if (!is_null($Category)) {
                        $categoryDoBanco = $user->categories->contains('category', $Category);
                        // $contato->categories()->detach();
                        // dd($categoryDoBanco);
                        if (!$categoryDoBanco) {
                            $dadosCategory = [
                                'user_id' => $user->id,
                                'contato_id' => $contato->id,
                                'category' => $Category
                            ];
                            $newCategory = Category::create($dadosCategory);
                            $newCategory->contato()->attach($contato->id);
                        } else {
                            $categoryToAttach = $user->categories->where('category', $Category)->first();
                            if (!$categoryToAttach->belongsTo($contato)) {
                                $categoryToAttach->contato()->attach($contato->id);
                            }
                        }
                    }
                    if (!is_null($Cep) || !is_null($Cidade) || !is_null($UF) || !is_null($Rua) || !is_null($Bairro) || !is_null($Numero)) {
                        $dadosAddress = [
                            'contato_id' => $contato->id,
                            'city' => $Cidade,
                            'state' => $UF,
                            'street' => $Rua,
                            'neighborhood' => $Bairro,
                            'cep' => $Cep,
                            'number' => $Numero,
                        ];
                        Address::create($dadosAddress);
                    }


                    $contato->tels;
                    $contato->emails;
                    $contato->categories;
                    $contato->cpfs->first();
                    $contato->addresses;

                    return response()->json(['Todos os Dados' => $contato]);
                } else {
                    abort(403, 'Unauthorized');
                }
            } else {
                abort(403, 'Unauthorized');
            }
        } else {
            abort(403, 'Unauthorized');
        }
    }

    public function delete($id, Request $request)
    {
        $header = $request->header('token');
        if (!$header == "") {
            $user = User::where('api_token', $header)->first();
            $contato = $user->contatos->find($id);
            
            if ($user) {
                if (!is_null($contato)) {
                    $contato->forceDelete();
                    return response()->json('Contato removido:');
                } else {
                    return response()->json('Contato inexistente:');
                }
            }
        }
    }

    public function deleteDados($id, Request $request)
    {
        $header = $request->header('token');
        $Tel = $request->json("telId");
        $Email = $request->json("emailId");
        $Category = $request->json("categoryId");
        $Address = $request->json("addressId");
        $CPF = $request->json("cpfId");
        if (!$header == "") {
            $user = User::where('api_token', $header)->first();
            $contato = $user->contatos->find($id);
            
            if (!is_null($contato)) {
                if (!is_null($Tel)) {
                    $telefone=Tel::find($Tel);
                    if (!is_null($telefone)) {
                        $telefone->forceDelete();
                    }
                }
            }
            if (!is_null($contato)) {
                if (!is_null($Email)) {
                    $email=Email::find($Email);
                    if (!is_null($email)) {
                        $email->forceDelete();
                    }
                } else {
                    return response()->json('Email inexistente:');
                }
            }
            if (!is_null($contato)) {
                if (!is_null($Category)) {
                    $category=Category::find($Category);
                    if (!is_null($category)) {
                        $category->forceDelete();
                    }
                } else {
                    return response()->json('Categoria inexistente:');
                }
            }
            if (!is_null($contato)) {
                if (!is_null($Address)) {
                    $address=Address::find($Address);
                    if (!is_null($address)) {
                        $address->forceDelete();
                    }
                } else {
                    return response()->json('Endereço inexistente:');
                }
            }
            if (!is_null($contato)) {
                if (!is_null($CPF)) {
                    $cpf=CPF::find($CPF);
                    if (!is_null($cpf)) {
                        $cpf->forceDelete();
                    }
                } else {
                    return response()->json('Cpf inexistente:');
                }
            }
            $contato->tels;
            $contato->emails;
            $contato->categories;
            $contato->cpfs->first();
            $contato->addresses;
            return response()->json(['Contato:', $contato]);
        }
    }
}
