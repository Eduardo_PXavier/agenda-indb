<?php

namespace App\Http\Controllers;

use App\Address;
use App\Contato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ContatoRequest;

class AddressController extends Controller
{
    public function postAddress(
        array $newCitys,
        array $states,
        array $ceps,
        array $streets,
        array $neighborhoods,
        array $numbers,
        int $id
    ) {
        // dd($newCitys);

        for ($i = 0; $i < count($newCitys); ++$i) {
            // dd($newCitys);
            if (!is_null($newCitys[$i]) || !is_null($states[$i]) || !is_null($streets[$i]) || !is_null($neighborhoods[$i]) || !is_null($ceps[$i]) || !is_null($numbers[$i])) {
                // dd($newCitys);
                $dadosAddress = [

                    'contato_id' => $id,
                    'city' => $newCitys[$i],
                    'state' => $states[$i],
                    'street' => $streets[$i],
                    'neighborhood' => $neighborhoods[$i],
                    'cep' => $ceps[$i],
                    'number' => $numbers[$i],

                ];

                Address::create($dadosAddress);
            }
        }
    }
    public function deleteAddress($id)
    {
        $address = Address::findOrFail($id);
        $address->delete();
    }

    public function updateAddress(
        array $citys,
        array $states,
        array $ceps,
        array $streets,
        array $neighborhoods,
        array $numbers,
        int $contactId
    ) {
        // dd($ceps);

        $valorCity = false;
        $valorState = false;
        $valorCep = false;
        $valorStreet = false;
        $valorNeighborhood = false;
        $valorNumber = false;





        foreach ($citys as $id => $city) {
            $cityDoBanco = Address::find($id);
            // dd($cityDoBanco);
            if (!is_null($cityDoBanco)) {
                $cityDoBanco->city = $city;
                $cityDoBanco->save();
            }

            if (is_null($city)) {
                $valorCity = true;
            }
        }

        foreach ($states as $id => $state) {
            $stateDoBanco = Address::find($id);
            if (!is_null($stateDoBanco)) {
                $stateDoBanco->state = $state;
                $stateDoBanco->save();
            }
            if (is_null($state)) {
                $valorState = true;
            }
        }

        foreach ($ceps as $id => $cep) {
            $cepDoBanco = Address::find($id);
            if (!is_null($cepDoBanco)) {
                $cepDoBanco->cep = $cep;
                $cepDoBanco->save();
            }
            if (is_null($cep)) {
                $valorCep = true;
            }
            // dd($id);
        }

        foreach ($streets as $id => $street) {
            // dd($street);
            $streetDoBanco = Address::find($id);
            if (!is_null($streetDoBanco)) {
                $streetDoBanco->street = $street;
                $streetDoBanco->save();
            }
            if (is_null($street)) {
                $valorStreet = true;
            }
        }

        foreach ($neighborhoods as $id => $neighborhood) {
            $neighborhoodDoBanco = Address::find($id);
            if (!is_null($neighborhoodDoBanco)) {
                $neighborhoodDoBanco->neighborhood = $neighborhood;
                $neighborhoodDoBanco->save();
            }

            if (is_null($neighborhood)) {
                $valorNeighborhood = true;
                // dd($valorNeighborhood);
            }
        }

        foreach ($numbers as $id => $number) {
            $numberDoBanco = Address::find($id);
            if (!is_null($numberDoBanco)) {
                $numberDoBanco->number = $number;
                $numberDoBanco->save();
            }
            if (is_null($number)) {
                $valorNumber = true;
            }

            // dd($valorId);
        }

        // dd($valorCity &&  $valorState && $valorCep && $valorNeighborhood && $valorNumber && $valorStreet);
        // dd(Address::find($contactId));

    }
}
