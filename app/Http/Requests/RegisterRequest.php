<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ];
    }

    /**
     * Mostra mensagens de erro
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'nome é obrigatório',
            'name.min' => 'mínimo de 3 letras para nome',
            'email.required'  => 'email é obrigatório',
            'email.unique'  => 'email já existe',
            'email.email' => 'o email tem que ser um email válido',
            'password.required_with' => 'senha é obrigatória',
            'password.min'=>'numero mínimo de caractéres para senha tem que ser 6',
            'password.same'=>'senha tem que ser igual',
            'password_confirmation.min'=>'numero mínimo de caractéres para senha tem que ser 6'
        ];
    }
}
