<?php

namespace App\Exports;

use App\Contato;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Concerns\FromCollection;

class ContactsExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function view(): View
    {
        $contacts = Auth::user()->contatos;
        return view('paginas.exportando', [
            'contatos' => $contacts->all()
        ]);
    }
}
