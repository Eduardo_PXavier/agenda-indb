<?php

namespace App;

use App\Contato;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'city', 'street', 'neighborhood', 'number', 'state', 'cep', 'contato_id'
    ];

    

    public function contato()
    {
        return $this->belongsTo(Contato::class);
    }

}