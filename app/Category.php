<?php

namespace App;

use App\Contato;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = [
        'category','user_id'
    ];

    

    public function contato()
    {
        return $this->belongsToMany(Contato::class, 'category_contact', 'category_id', 'contact_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}