<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('cadastrar', 'RegistrationController@cadastrar');
Route::post('cadastrar', 'RegistrationController@postRegister');

Route::get('entrar', 'SessionsController@login');
Route::post('entrar', 'SessionsController@postLogin');
Route::get('logout', 'SessionsController@logout')->name('logout');

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::resource('contato', 'ContatoController');
    Route::get('contatos', 'CadastradoController@contatos')->name('contatos');

    

    //categoria
    Route::get('categoria', 'CategoryController@index');
    Route::get('contatos/{categoria}', 'CategoryController@contatosCategoria')->name('pesquisaCategoria');
    // lixeira
    Route::get('lixeira', 'ContatoController@trash')->name('lixeira');
    Route::post('restore/{contato}', 'ContatoController@restore')->name('restore');
    Route::post('exclui/{contato}', 'ContatoController@forceDestroy')->name('exclui');

    // Routes de botoes delete(-) em edit
    Route::delete('delete/tel/{id}', 'TelController@deleteTel');
    Route::delete('delete/email/{id}', 'EmailController@deleteEmail');
    Route::delete('delete/categoria/{id}/{contato}', 'CategoryController@deleteCategory');
    Route::delete('delete/endereco/{id}', 'AddressController@deleteAddress');

    Route::post('restoreAll', 'ContatoController@restoreAll');
    Route::get('/', 'ContatoController@index');

    Route::get('events', 'EventController@index')->name('events.index');
    Route::resource('events', 'EventController');
    // Route::get('events/list', 'EventController@listEvent')->name('events.list');
    // Route::delete('event/{eventId}', 'EventController@deleteEvent')->name('events.delete');

    Route::get('contact/import/google', ['as' => 'google.import', 'uses' => 'ContatoController@importGoogleContact']);
    Route::post('contatos/google', 'ContatoController@importadosGoogle');
    Route::get('excel', 'ContatoController@excel');
    Route::get('contacts/export/', 'ContatoController@export');

    Route::get('/search', 'SearchController@search');
});

Route::get('terms', function () {
    return view('termos.terms');
});

Route::get('terms2', function () {
    return view('termos.terms2');
});

Route::get('primeira', function () {
    return view('paginas.primeira');
});

Route::get('teste', function () {
    return view('auth.teste');
});


// background-size:cover;