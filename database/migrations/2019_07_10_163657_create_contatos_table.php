<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('last_name');
            // $table->string('email');
            // $table->string('telefone1');
            // $table->string('telefone2');
            // $table->string('cidade');
            // $table->string('estado');
            // $table->string('cep');
            // $table->string('rua');
            // $table->string('bairro');
            // $table->string('numero');
            // $table->string('relacao');
            $table->timestamps();
        });
        Schema::table('contatos', function ($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
